package com.ericvogler.CubePractice01;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;

public class RandomSolver {

	public static ArrayList<Alg> randomSolution(Cube cube, Alg cross,
			PossibleNextF2LSteps pns) {
		Random random = new Random();
		ArrayList<Alg> result = new ArrayList<Alg>();
		Cube workingCube = new Cube(cube);

		result.add(cross);
		workingCube.transform(cross);

		while (PossibleNextF2LSteps.unsolvedSlots(workingCube).size() > 0) {
			ArrayList<Alg> nextAlgs = pns.getPossibilities(workingCube);
			if (nextAlgs.size() == 0) {
				return null;
			}
			Alg nextAlg = PossibleNextF2LSteps.randomAlg(nextAlgs);
			result.add(nextAlg);
			workingCube.transform(nextAlg);
		}

		return result;
	}

	public static Alg combinedAlg(ArrayList<Alg> algs) {
		Alg result = new Alg(algs);
		result = Cancel.cancelledAlg(result);
		return result;
	}

	public static void main(String[] args) {
		Alg scramble = new Alg(
				"F2 L2 B2 R2 D U2 R2 U' F2 D B2 L' B' F2 D F2 U R2 U");
		Cube cube = new Cube(scramble);
		System.out.println(scramble);

		int bestCount = 9999;

		ArrayList<Alg> solution = new ArrayList<Alg>();
		ArrayList<Alg> tempSolution;
		Cube workingCube;
		ArrayList<Alg> crosses = XCross.getCrosses(cube, 2);

		PossibleNextF2LSteps pns = new PossibleNextF2LSteps(
				F2lMoves.allDoubleAlgs());

		int findCount = 0;
		HashSet<Alg> foundAlgs = new HashSet<Alg>();
		MoveCountCounter moveCountCounter = new MoveCountCounter();
		LastLayerConditions lastLayerBasicConditions = new LastLayerBasicConditions(
				true, false, false, false, true);
		int iterationCount = 100 * 1000 * 1000;
		int printoutThreshhold = 10000;
		for (int i = 0; i < iterationCount; i++) {
			if (i % printoutThreshhold == 0 && i != 0) {
				System.out.print("Attempts: ");
				System.out.println(i);
				System.out.print("unique percentage: ");
				System.out.println((double) foundAlgs.size() / (double) i);
				System.out.println(moveCountCounter);
				printoutThreshhold = printoutThreshhold * 2;
			}
			Alg cross = PossibleNextF2LSteps.randomAlg(crosses);
			tempSolution = randomSolution(cube, cross, pns);
			if (tempSolution == null) {
				continue;
			}
			Alg nextAlg = combinedAlg(tempSolution);
			workingCube = new Cube(scramble, nextAlg);

			// if (!solveConditions.isSatisfactory(workingCube)) {
			// continue;
			// }

			findCount++;
			foundAlgs.add(nextAlg);
			moveCountCounter.count(nextAlg.getLength());

			int mc = nextAlg.getLength();
			if (mc < bestCount) {
				solution = tempSolution;
				bestCount = mc;
				System.out.println();
				System.out.print("Solve count: ");
				System.out.println(i);
				System.out.println(bestCount);
				System.out.println(nextAlg);
			}
		}

		System.out.println("done.");
		System.out.println(findCount);
	}
}

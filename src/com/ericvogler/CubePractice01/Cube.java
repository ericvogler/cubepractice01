package com.ericvogler.CubePractice01;

import java.util.Arrays;

public class Cube {
	protected Edge[] edgePermutation;
	protected Corner[] cornerPermutation;
	protected Flip[] edgeOrientation;
	protected Twist[] cornerOrientation;
	protected byte centerOrientation;

	// XXX: getters provide references instead of copies?
	public Edge[] getEdgePermutation() {
		return edgePermutation;
	}

	public void setEdgePermutation(Edge[] edgePermutation) {
		this.edgePermutation = edgePermutation;
	}

	public Corner[] getCornerPermutation() {
		return cornerPermutation;
	}

	public void setCornerPermutation(Corner[] cornerPermutation) {
		this.cornerPermutation = cornerPermutation;
	}

	public Flip[] getEdgeOrientation() {
		return edgeOrientation;
	}

	public void setEdgeOrientation(Flip[] edgeOrientation) {
		this.edgeOrientation = edgeOrientation;
	}

	public Twist[] getCornerOrientation() {
		return cornerOrientation;
	}

	public void setCornerOrientation(Twist[] cornerOrientation) {
		this.cornerOrientation = cornerOrientation;
	}

	public byte getCenterOrientation() {
		return centerOrientation;
	}

	public void setCenterOrientation(byte centerOrientation) {
		this.centerOrientation = centerOrientation;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + centerOrientation;
		result = prime * result + Arrays.hashCode(cornerOrientation);
		result = prime * result + Arrays.hashCode(cornerPermutation);
		result = prime * result + Arrays.hashCode(edgeOrientation);
		result = prime * result + Arrays.hashCode(edgePermutation);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cube other = (Cube) obj;
		if (centerOrientation != other.centerOrientation)
			return false;
		if (!Arrays.equals(cornerOrientation, other.cornerOrientation))
			return false;
		if (!Arrays.equals(cornerPermutation, other.cornerPermutation))
			return false;
		if (!Arrays.equals(edgeOrientation, other.edgeOrientation))
			return false;
		if (!Arrays.equals(edgePermutation, other.edgePermutation))
			return false;
		return true;
	}

	private void setState(Edge[] edgePermutation, Corner[] cornerPermutation,
			Flip[] edgeOrientation, Twist[] cornerOrientation,
			byte centerOrientation) {
		this.edgePermutation = edgePermutation;
		this.cornerPermutation = cornerPermutation;
		this.edgeOrientation = edgeOrientation;
		this.cornerOrientation = cornerOrientation;
		this.centerOrientation = centerOrientation;
	}

	private void setStateToSolvedCube() {
		Flip[] orientedEdges = new Flip[12];
		Arrays.fill(orientedEdges, Flip.GOOD);
		Twist[] orientedCorners = new Twist[8];
		Arrays.fill(orientedCorners, Twist.GOOD);
		setState(Edge.values(), Corner.values(), orientedEdges,
				orientedCorners, (byte) 0);
	}

	public Cube() {
		setStateToSolvedCube();
	}

	public Cube(Alg... algs) {
		setStateToSolvedCube();
		for (Alg a : algs) {
			transform(a);
		}
	}

	public Cube(Cube cubeToCopy) {
		this.edgePermutation = Arrays.copyOf(cubeToCopy.edgePermutation, 12);
		this.cornerPermutation = Arrays.copyOf(cubeToCopy.cornerPermutation, 8);
		this.edgeOrientation = Arrays.copyOf(cubeToCopy.edgeOrientation, 12);
		this.cornerOrientation = Arrays.copyOf(cubeToCopy.cornerOrientation, 8);
		this.centerOrientation = cubeToCopy.centerOrientation;
	}

	public void transform(Alg alg) {
		Transformations transformations = Transformations.getInstance();
		Transformation transformation = transformations.getTransformation(alg);
		transform(transformation);
		// for (Move move : alg.getMoves()) {
		// Transformation transformation =
		// transformations.getTransformation(move);
		// transform(transformation);
		// }
	}

	public void transform(String notation) {
		transform(new Alg(notation));
	}

	public void transform(Move move) {
		transform(Transformations.getInstance().getTransformation(move));
	}

	public void transform(Transformation transformation) {
		Edge[] newEdges = new Edge[12];
		Flip[] newFlips = new Flip[12];
		for (int i = 0; i < 12; i++) {
			newEdges[i] = this.edgePermutation[transformation.edgePermutation[i]];
			newFlips[i] = this.edgeOrientation[transformation.edgePermutation[i]];
			if (newFlips[i] != null) {
				if (transformation.edgeOrientation[i] == 1) {
					switch (newFlips[i]) {
					case GOOD:
						newFlips[i] = Flip.FLIPPED;
						break;
					case FLIPPED:
						newFlips[i] = Flip.GOOD;
					}
				}
			}
		}
		Corner[] newCorners = new Corner[8];
		Twist[] newTwists = new Twist[8];
		for (int i = 0; i < 8; i++) {
			newCorners[i] = this.cornerPermutation[transformation.cornerPermutation[i]];
			newTwists[i] = this.cornerOrientation[transformation.cornerPermutation[i]];
			if (newTwists[i] != null) {
				switch (transformation.cornerOrientation[i]) {
				case 0:
					break;
				case 1:
					switch (newTwists[i]) {
					case GOOD:
						newTwists[i] = Twist.CW;
						break;
					case CW:
						newTwists[i] = Twist.CCW;
						break;
					case CCW:
						newTwists[i] = Twist.GOOD;
						break;
					}
					break;
				case 2:
					switch (newTwists[i]) {
					case GOOD:
						newTwists[i] = Twist.CCW;
						break;
					case CW:
						newTwists[i] = Twist.GOOD;
						break;
					case CCW:
						newTwists[i] = Twist.CW;
						break;
					}
				}
			}
		}
		setState(newEdges, newCorners, newFlips, newTwists, centerOrientation);
	}

	public void printState() {
		System.out.println(Arrays.toString(this.edgePermutation));
		System.out.println(Arrays.toString(this.cornerPermutation));
		System.out.println(Arrays.toString(this.edgeOrientation));
		System.out.println(Arrays.toString(this.cornerOrientation));
		System.out.println(this.centerOrientation);
	}

	public static void main(String[] args) {
		Cube cube = new Cube();
		Cube solved = new Cube();
		cube.printState();
		Alg alg = new Alg("R D L F B U");
		cube.transform(alg);
		cube.printState();
	}

	public EdgeLocation getEdgeLocation(Edge edge) {
		int pos = 0;
		boolean found = false;
		do {
			if (this.edgePermutation[pos] == edge) {
				found = true;
			} else {
				pos++;
			}
		} while (!found);
		// return null for masked cube? or cause an exceptioN?
		return found ? new EdgeLocation(pos, this.edgeOrientation[pos]) : null;
	}

	public CornerLocation getCornerLocation(Corner corner) {
		int pos = 0;
		boolean found = false;
		do {
			if (this.cornerPermutation[pos] == corner) {
				found = true;
			} else {
				pos++;
			}
		} while (!found);
		return found ? new CornerLocation(pos, this.cornerOrientation[pos])
				: null;
	}

	public boolean pieceIsSolved(Piece piece) {
		if (piece instanceof Edge) {
			Edge edge = (Edge) piece;
			if (!((this.edgePermutation[edge.ordinal()] == edge) && (this.edgeOrientation[edge
					.ordinal()] == Flip.GOOD))) {
				return false;
			}
		} else {
			Corner corner = (Corner) piece;
			if (!((this.cornerPermutation[corner.ordinal()] == corner) && (this.cornerOrientation[corner
					.ordinal()] == Twist.GOOD))) {
				return false;
			}
		}
		return true;
	}

	public boolean piecesAreSolved(Piece[] pieces) {
		for (Piece piece : pieces) {
			if (!pieceIsSolved(piece)) {
				return false;
			}
		}
		return true;
	}
}

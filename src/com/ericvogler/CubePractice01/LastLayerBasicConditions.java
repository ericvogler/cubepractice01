package com.ericvogler.CubePractice01;

import java.util.ArrayList;
import java.util.Arrays;

public class LastLayerBasicConditions implements LastLayerConditions {
	private static final Flip[] solvedEdgeOrientation;
	private static final Twist[] solvedCornerOrientation;
	private static final Edge[] solvedEdgePermutation;
	private static final Corner[] solvedCornerPermutation;

	private boolean checkEO;
	private boolean checkCO;
	private boolean checkEP;
	private boolean checkCP;
	private boolean allowAuf;

	static {
		Cube solvedCube = new Cube();
		solvedEdgeOrientation = solvedCube.getEdgeOrientation();
		solvedCornerOrientation = solvedCube.getCornerOrientation();
		solvedEdgePermutation = solvedCube.getEdgePermutation();
		solvedCornerPermutation = solvedCube.getCornerPermutation();
	}

	public LastLayerBasicConditions(boolean checkEO, boolean checkCO, boolean checkEP,
			boolean checkCP, boolean checkAuf) {
		super();
		this.checkEO = checkEO;
		this.checkCO = checkCO;
		this.checkEP = checkEP;
		this.checkCP = checkCP;
		this.allowAuf = checkAuf;
	}

	/* (non-Javadoc)
	 * @see com.ericvogler.CubePractice01.LastLayerConditions#isSatisfactory(com.ericvogler.CubePractice01.Cube)
	 */
	@Override
	public boolean isSatisfactory(Cube cube) {
		Move[] aufs = (allowAuf) ? new Move[] { Move.U, Move.U2, Move.Up, null }
				: new Move[] { null };
		Cube workingCube;
		for (Move auf : aufs) {
			boolean thisOneIsSatisfactory = true;
			workingCube = new Cube(cube);
			if (auf != null) {
				workingCube.transform(auf);
			}
			if (checkEO && !isEdgeOrientationSolved(workingCube))
				thisOneIsSatisfactory = false;
			if (checkCO && !isCornerOrientationSolved(workingCube))
				thisOneIsSatisfactory = false;
			if (checkEP && !isEdgePermutationSolved(workingCube))
				thisOneIsSatisfactory = false;
			if (checkCP && !isCornerPermutationSolved(workingCube))
				thisOneIsSatisfactory = false;
			if (thisOneIsSatisfactory) {
				return true;
			}
		}
		return false;
	}

	public ArrayList<Alg> getAlgs(Cube cube) {
		ArrayList<Alg> result = new ArrayList<Alg>();
		result.add(new Alg(""));
		return result;
	}
	
	private static boolean isEdgeOrientationSolved(Cube cube) {
		return (Arrays.equals(cube.getEdgeOrientation(), solvedEdgeOrientation)) ? true
				: false;
	}

	private static boolean isEdgePermutationSolved(Cube cube) {
		return (Arrays.equals(cube.getEdgePermutation(), solvedEdgePermutation)) ? true
				: false;
	}

	private static boolean isCornerOrientationSolved(Cube cube) {
		return (Arrays.equals(cube.getCornerOrientation(),
				solvedCornerOrientation)) ? true : false;
	}

	private static boolean isCornerPermutationSolved(Cube cube) {
		return (Arrays.equals(cube.getCornerPermutation(),
				solvedCornerPermutation)) ? true : false;
	}

	public static void main(String[] args) {
		Cube cube = new Cube(
				new Alg(
						"R U2 F' L' D' B' R' U' F2 L' D' B2 R U' F L U' B R D' F' D2 L2 U R2 B2 U' B' U2 L U2 L' F U F' R U' R' U2 R' U' R U' R' U' R L' U' L"));
		LastLayerConditions conditions = new LastLayerBasicConditions(false,  true, false, true, true);
		System.out.println(conditions.isSatisfactory(cube));
	}
}

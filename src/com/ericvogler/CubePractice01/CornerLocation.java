package com.ericvogler.CubePractice01;

public class CornerLocation {
	public int cornerPermutation;
	public Twist cornerOrientation;

	public CornerLocation(int cornerPermutation, Twist cornerOrientation) {
		super();
		this.cornerPermutation = cornerPermutation;
		this.cornerOrientation = cornerOrientation;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((cornerOrientation == null) ? 0 : cornerOrientation
						.hashCode());
		result = prime * result + cornerPermutation;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CornerLocation other = (CornerLocation) obj;
		if (cornerOrientation != other.cornerOrientation)
			return false;
		if (cornerPermutation != other.cornerPermutation)
			return false;
		return true;
	}
}

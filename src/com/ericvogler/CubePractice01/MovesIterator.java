/*XXX: If last permutation is redundant (which it is), first one 
 * gets returned before isFinished realizes it's finished.
 */

package com.ericvogler.CubePractice01;

import java.util.Arrays;
import java.util.HashMap;

public class MovesIterator {

	private int moveCount;
	private Move[] position;
	private static final int numberOfMoveTypes = 18;
	private long incrementCounter = 0;
	private long finalIncrement;
	private Move[] moves = { Move.R, Move.R2, Move.Rp, Move.L, Move.L2,
			Move.Lp, Move.U, Move.U2, Move.Up, Move.D, Move.D2, Move.Dp,
			Move.F, Move.F2, Move.Fp, Move.B, Move.B2, Move.Bp };
	private HashMap<Move, Move> nextMove;
	private Move lastMove;

	public MovesIterator(int moveCount) {
		this.moveCount = moveCount;
		this.position = new Move[moveCount];
		for (int i = 0; i < moveCount; i++) {
			this.position[i] = moves[0];
		}
		this.finalIncrement = (long) Math
				.pow(numberOfMoveTypes, this.moveCount);
		this.lastMove = moves[moves.length - 1];
		makeNextMoves();
	}

	private void makeNextMoves() {
		nextMove = new HashMap<Move, Move>();
		for (int i = 0; i < (moves.length - 1); i++) {
			nextMove.put(moves[i], moves[i + 1]);
		}
		nextMove.put(moves[moves.length - 2], moves[moves.length - 1]);
	}

	private void increment() {
		do
			_increment();
		while (isRedundant(this.position));

	}

	private void _increment() {

		int pos = 0;
		while (true) {
			if (this.position[pos] != this.lastMove) {
				break;
			}
			pos += 1;
			if (pos == this.moveCount) {
				for (int i = 0; i < this.moveCount; i++)
					this.position[i] = moves[0];
				return;
			}

		}

		this.position[pos] = (Move) nextMove.get(this.position[pos]);
		if (pos > 0) {
			for (int i = 0; i < pos; i++)
				this.position[i] = moves[0];

		}
		this.incrementCounter += 1;
	}

	private boolean isRedundant(Move[] moves) {
		for (int i = 1; i < moves.length; i++) {
			switch (moves[i - 1]) {
			case R:
			case R2:
			case Rp:
				if (moves[i] == Move.R || moves[i] == Move.R2
						|| moves[i] == Move.Rp)
					return true;
				break;
			case L:
			case L2:
			case Lp:
				if ((moves[i] == Move.R) || (moves[i] == Move.R2)
						|| (moves[i] == Move.Rp) || (moves[i] == Move.L)
						|| (moves[i] == Move.L2) || (moves[i] == Move.Lp))
					return true;
				break;

			case U:
			case U2:
			case Up:
				if ((moves[i] == Move.U) || (moves[i] == Move.U2)
						|| (moves[i] == Move.Up))
					return true;
				break;
			case D:
			case D2:
			case Dp:
				if (moves[i] == Move.U || moves[i] == Move.U2
						|| moves[i] == Move.Up || moves[i] == Move.D
						|| moves[i] == Move.D2 || moves[i] == Move.Dp)
					return true;
				break;

			case F:
			case F2:
			case Fp:
				if (moves[i] == Move.F || moves[i] == Move.F2
						|| moves[i] == Move.Fp)
					return true;
				break;
			case B:
			case B2:
			case Bp:
				if (moves[i] == Move.F || moves[i] == Move.F2
						|| moves[i] == Move.Fp || moves[i] == Move.B
						|| moves[i] == Move.B2 || moves[i] == Move.Bp)
					return true;
				break;
			}
		}
		return false;
	}

	public boolean isFinished() {
		if (this.incrementCounter >= (this.finalIncrement - 1))
			return true;
		else
			return false;
	}

	public Move[] numbersToMoves(byte[] numbers) {
		Move[] moves = new Move[this.moveCount];
		for (int i = 0; i < this.moveCount; i++) {
			moves[i] = this.moves[numbers[i]];
		}
		return moves;
	}

	public Move[] getNext() {
		Move[] result = position;
		this.increment();

		return result;
	}

	public static void main(String[] args) {

		MovesIterator mi = new MovesIterator(3);
		Move[] n;
		Alg a;
		do {
			n = mi.getNext();
			a = new Alg(n);
			System.out.println(a.getNotation());
		} while (!mi.isFinished());
	}
}

//XXX: OMG I made a singleton! I feel so naughty.
package com.ericvogler.CubePractice01;

import java.util.HashMap;

public class Transformations {
	private static HashMap<Move, Transformation> singleMoveMap = new HashMap<Move, Transformation>();
	private static HashMap<Alg, Transformation> algMap = new HashMap<Alg, Transformation>();
	private static int lookUpCount = 0;
	private static boolean cacheResults = true;

	static {
		singleMoveMap.put(Move.R, new Transformation(new byte[] { 0, 8, 2, 3,
				4, 10, 6, 7, 5, 9, 1, 11 },
				new byte[] { 4, 0, 2, 3, 7, 5, 6, 1 }, new byte[] { 0, 0, 0, 0,
						0, 0, 0, 0, 0, 0, 0, 0 }, new byte[] { 2, 1, 0, 0, 1,
						0, 0, 2 }, new byte[] { 0 }));
		singleMoveMap.put(Move.R2, new Transformation(new byte[] { 0, 5, 2, 3,
				4, 1, 6, 7, 10, 9, 8, 11 },
				new byte[] { 7, 4, 2, 3, 1, 5, 6, 0 }, new byte[] { 0, 0, 0, 0,
						0, 0, 0, 0, 0, 0, 0, 0 }, new byte[] { 0, 0, 0, 0, 0,
						0, 0, 0 }, new byte[] { 0 }));
		singleMoveMap.put(Move.Rp, new Transformation(new byte[] { 0, 10, 2, 3,
				4, 8, 6, 7, 1, 9, 5, 11 },
				new byte[] { 1, 7, 2, 3, 0, 5, 6, 4 }, new byte[] { 0, 0, 0, 0,
						0, 0, 0, 0, 0, 0, 0, 0 }, new byte[] { 2, 1, 0, 0, 1,
						0, 0, 2 }, new byte[] { 0 }));
		singleMoveMap.put(Move.L, new Transformation(new byte[] { 0, 1, 2, 11,
				4, 5, 6, 9, 8, 3, 10, 7 },
				new byte[] { 0, 1, 6, 2, 4, 3, 5, 7 }, new byte[] { 0, 0, 0, 0,
						0, 0, 0, 0, 0, 0, 0, 0 }, new byte[] { 0, 0, 2, 1, 0,
						2, 1, 0 }, new byte[] { 0 }));
		singleMoveMap.put(Move.L2, new Transformation(new byte[] { 0, 1, 2, 7,
				4, 5, 6, 3, 8, 11, 10, 9 },
				new byte[] { 0, 1, 5, 6, 4, 2, 3, 7 }, new byte[] { 0, 0, 0, 0,
						0, 0, 0, 0, 0, 0, 0, 0 }, new byte[] { 0, 0, 0, 0, 0,
						0, 0, 0 }, new byte[] { 0 }));
		singleMoveMap.put(Move.Lp, new Transformation(new byte[] { 0, 1, 2, 9,
				4, 5, 6, 11, 8, 7, 10, 3 },
				new byte[] { 0, 1, 3, 5, 4, 6, 2, 7 }, new byte[] { 0, 0, 0, 0,
						0, 0, 0, 0, 0, 0, 0, 0 }, new byte[] { 0, 0, 2, 1, 0,
						2, 1, 0 }, new byte[] { 0 }));
		singleMoveMap.put(Move.U, new Transformation(new byte[] { 1, 2, 3, 0,
				4, 5, 6, 7, 8, 9, 10, 11 },
				new byte[] { 1, 2, 3, 0, 4, 5, 6, 7 }, new byte[] { 0, 0, 0, 0,
						0, 0, 0, 0, 0, 0, 0, 0 }, new byte[] { 0, 0, 0, 0, 0,
						0, 0, 0 }, new byte[] { 0 }));
		singleMoveMap.put(Move.U2, new Transformation(new byte[] { 2, 3, 0, 1,
				4, 5, 6, 7, 8, 9, 10, 11 },
				new byte[] { 2, 3, 0, 1, 4, 5, 6, 7 }, new byte[] { 0, 0, 0, 0,
						0, 0, 0, 0, 0, 0, 0, 0 }, new byte[] { 0, 0, 0, 0, 0,
						0, 0, 0 }, new byte[] { 0 }));
		singleMoveMap.put(Move.Up, new Transformation(new byte[] { 3, 0, 1, 2,
				4, 5, 6, 7, 8, 9, 10, 11 },
				new byte[] { 3, 0, 1, 2, 4, 5, 6, 7 }, new byte[] { 0, 0, 0, 0,
						0, 0, 0, 0, 0, 0, 0, 0 }, new byte[] { 0, 0, 0, 0, 0,
						0, 0, 0 }, new byte[] { 0 }));
		singleMoveMap.put(Move.D, new Transformation(new byte[] { 0, 1, 2, 3,
				7, 4, 5, 6, 8, 9, 10, 11 },
				new byte[] { 0, 1, 2, 3, 5, 6, 7, 4 }, new byte[] { 0, 0, 0, 0,
						0, 0, 0, 0, 0, 0, 0, 0 }, new byte[] { 0, 0, 0, 0, 0,
						0, 0, 0 }, new byte[] { 0 }));
		singleMoveMap.put(Move.D2, new Transformation(new byte[] { 0, 1, 2, 3,
				6, 7, 4, 5, 8, 9, 10, 11 },
				new byte[] { 0, 1, 2, 3, 6, 7, 4, 5 }, new byte[] { 0, 0, 0, 0,
						0, 0, 0, 0, 0, 0, 0, 0 }, new byte[] { 0, 0, 0, 0, 0,
						0, 0, 0 }, new byte[] { 0 }));
		singleMoveMap.put(Move.Dp, new Transformation(new byte[] { 0, 1, 2, 3,
				5, 6, 7, 4, 8, 9, 10, 11 },
				new byte[] { 0, 1, 2, 3, 7, 4, 5, 6 }, new byte[] { 0, 0, 0, 0,
						0, 0, 0, 0, 0, 0, 0, 0 }, new byte[] { 0, 0, 0, 0, 0,
						0, 0, 0 }, new byte[] { 0 }));
		singleMoveMap.put(Move.F, new Transformation(new byte[] { 9, 1, 2, 3,
				8, 5, 6, 7, 0, 4, 10, 11 },
				new byte[] { 3, 1, 2, 5, 0, 4, 6, 7 }, new byte[] { 1, 0, 0, 0,
						1, 0, 0, 0, 1, 1, 0, 0 }, new byte[] { 1, 0, 0, 2, 2,
						1, 0, 0 }, new byte[] { 0 }));
		singleMoveMap.put(Move.F2, new Transformation(new byte[] { 4, 1, 2, 3,
				0, 5, 6, 7, 9, 8, 10, 11 },
				new byte[] { 5, 1, 2, 4, 3, 0, 6, 7 }, new byte[] { 0, 0, 0, 0,
						0, 0, 0, 0, 0, 0, 0, 0 }, new byte[] { 0, 0, 0, 0, 0,
						0, 0, 0 }, new byte[] { 0 }));
		singleMoveMap.put(Move.Fp, new Transformation(new byte[] { 8, 1, 2, 3,
				9, 5, 6, 7, 4, 0, 10, 11 },
				new byte[] { 4, 1, 2, 0, 5, 3, 6, 7 }, new byte[] { 1, 0, 0, 0,
						1, 0, 0, 0, 1, 1, 0, 0 }, new byte[] { 1, 0, 0, 2, 2,
						1, 0, 0 }, new byte[] { 0 }));
		singleMoveMap.put(Move.B, new Transformation(new byte[] { 0, 1, 10, 3,
				4, 5, 11, 7, 8, 9, 6, 2 },
				new byte[] { 0, 7, 1, 3, 4, 5, 2, 6 }, new byte[] { 0, 0, 1, 0,
						0, 0, 1, 0, 0, 0, 1, 1 }, new byte[] { 0, 2, 1, 0, 0,
						0, 2, 1 }, new byte[] { 0 }));
		singleMoveMap.put(Move.B2, new Transformation(new byte[] { 0, 1, 6, 3,
				4, 5, 2, 7, 8, 9, 11, 10 },
				new byte[] { 0, 6, 7, 3, 4, 5, 1, 2 }, new byte[] { 0, 0, 0, 0,
						0, 0, 0, 0, 0, 0, 0, 0 }, new byte[] { 0, 0, 0, 0, 0,
						0, 0, 0 }, new byte[] { 0 }));
		singleMoveMap.put(Move.Bp, new Transformation(new byte[] { 0, 1, 11, 3,
				4, 5, 10, 7, 8, 9, 2, 6 },
				new byte[] { 0, 2, 6, 3, 4, 5, 7, 1 }, new byte[] { 0, 0, 1, 0,
						0, 0, 1, 0, 0, 0, 1, 1 }, new byte[] { 0, 2, 1, 0, 0,
						0, 2, 1 }, new byte[] { 0 }));
		singleMoveMap.put(Move.x, new Transformation(new byte[] { 4, 8, 0, 9,
				6, 10, 2, 11, 5, 7, 1, 3 },
				new byte[] { 4, 0, 3, 5, 7, 6, 2, 1 }, new byte[] { 1, 0, 1, 0,
						1, 0, 1, 0, 0, 0, 0, 0 }, new byte[] { 2, 1, 2, 1, 1,
						2, 1, 2 }, new byte[] { 0 }));
		singleMoveMap.put(Move.x2, new Transformation(new byte[] { 6, 5, 4, 7,
				2, 1, 0, 3, 10, 11, 8, 9 },
				new byte[] { 7, 4, 5, 6, 1, 2, 3, 0 }, new byte[] { 0, 0, 0, 0,
						0, 0, 0, 0, 0, 0, 0, 0 }, new byte[] { 0, 0, 0, 0, 0,
						0, 0, 0 }, new byte[] { 0 }));
		singleMoveMap.put(Move.xp, new Transformation(new byte[] { 2, 10, 6,
				11, 0, 8, 4, 9, 1, 3, 5, 7 }, new byte[] { 1, 7, 6, 2, 0, 3, 5,
				4 }, new byte[] { 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0 },
				new byte[] { 2, 1, 2, 1, 1, 2, 1, 2 }, new byte[] { 0 }));
		singleMoveMap.put(Move.y, new Transformation(new byte[] { 1, 2, 3, 0,
				5, 6, 7, 4, 10, 8, 11, 9 },
				new byte[] { 1, 2, 3, 0, 7, 4, 5, 6 }, new byte[] { 0, 0, 0, 0,
						0, 0, 0, 0, 1, 1, 1, 1 }, new byte[] { 0, 0, 0, 0, 0,
						0, 0, 0 }, new byte[] { 0 }));
		singleMoveMap.put(Move.y2, new Transformation(new byte[] { 2, 3, 0, 1,
				6, 7, 4, 5, 11, 10, 9, 8 },
				new byte[] { 2, 3, 0, 1, 6, 7, 4, 5 }, new byte[] { 0, 0, 0, 0,
						0, 0, 0, 0, 0, 0, 0, 0 }, new byte[] { 0, 0, 0, 0, 0,
						0, 0, 0 }, new byte[] { 0 }));
		singleMoveMap.put(Move.yp, new Transformation(new byte[] { 3, 0, 1, 2,
				7, 4, 5, 6, 9, 11, 8, 10 },
				new byte[] { 3, 0, 1, 2, 5, 6, 7, 4 }, new byte[] { 0, 0, 0, 0,
						0, 0, 0, 0, 1, 1, 1, 1 }, new byte[] { 0, 0, 0, 0, 0,
						0, 0, 0 }, new byte[] { 0 }));
		singleMoveMap.put(Move.z, new Transformation(new byte[] { 9, 3, 11, 7,
				8, 1, 10, 5, 0, 4, 2, 6 },
				new byte[] { 3, 2, 6, 5, 0, 4, 7, 1 }, new byte[] { 1, 1, 1, 1,
						1, 1, 1, 1, 1, 1, 1, 1 }, new byte[] { 1, 2, 1, 2, 2,
						1, 2, 1 }, new byte[] { 0 }));
		singleMoveMap.put(Move.z2, new Transformation(new byte[] { 4, 7, 6, 5,
				0, 3, 2, 1, 9, 8, 11, 10 },
				new byte[] { 5, 6, 7, 4, 3, 0, 1, 2 }, new byte[] { 0, 0, 0, 0,
						0, 0, 0, 0, 0, 0, 0, 0 }, new byte[] { 0, 0, 0, 0, 0,
						0, 0, 0 }, new byte[] { 0 }));
		singleMoveMap.put(Move.zp, new Transformation(new byte[] { 8, 5, 10, 1,
				9, 7, 11, 3, 4, 0, 6, 2 },
				new byte[] { 4, 7, 1, 0, 5, 3, 2, 6 }, new byte[] { 1, 1, 1, 1,
						1, 1, 1, 1, 1, 1, 1, 1 }, new byte[] { 1, 2, 1, 2, 2,
						1, 2, 1 }, new byte[] { 0 }));
	}

	private static Transformations instance = null;

	protected Transformations() {
	}

	public static Transformations getInstance() {
		if (instance == null) {
			instance = new Transformations();
		}
		return instance;
	}

	public static boolean isCacheResults() {
		return cacheResults;
	}

	public static void setCacheResults(boolean cacheResults) {
		Transformations.cacheResults = cacheResults;
	}

	public int getMapSize() {
		return algMap.size();
	}

	public static int getLookUpCount() {
		return lookUpCount;
	}

	public Transformation getTransformation(Move move) {
		return singleMoveMap.get(move);
	}

	public Transformation getTransformation(Alg alg) {
		// debug();

		if (alg.getLength() == 1) {
			return getTransformation(alg.getMoves()[0]);
		}

		if (!cacheResults) {
			return calculateTransformationUsingCube(alg);
		}

		if (algMap.containsKey(alg)) {
			return getCachedResult(alg);
		}

		Transformation result = calculateTransformationUsingCube(alg);
		algMap.put(alg, result);
		return result;
	}

	/*
	 * private Transformation calculateTransformationWithoutCube(Alg alg) { }
	 */

	private Transformation calculateTransformationUsingCube(Alg alg) {
		Alg reverse = alg; // alg.getReverse();
		Cube cube = new Cube();
		for (Move move : reverse.getMoves()) {
			cube.transform(getTransformation(move));
		}
		byte[] ep = new byte[12];
		byte[] eo = new byte[12];
		for (int i = 0; i < 12; i++) {
			ep[i] = (byte) cube.getEdgePermutation()[i].ordinal();
			eo[i] = (byte) cube.getEdgeOrientation()[i].ordinal();
		}
		byte[] cp = new byte[8];
		byte[] co = new byte[8];
		for (int i = 0; i < 8; i++) {
			cp[i] = (byte) cube.getCornerPermutation()[i].ordinal();
			co[i] = (byte) cube.getCornerOrientation()[i].ordinal();
		}
		byte[] center = { cube.getCenterOrientation() };
		Transformation result = new Transformation(ep, cp, eo, co, center);
		return result;
	}

	private void debug() {
		lookUpCount++;
		if (lookUpCount % 50000 == 0 && lookUpCount > 0) {
			System.out.print("Transformation look-up count: ");
			System.out.println(lookUpCount);
			System.out.print("Cache size: ");
			System.out.println(getMapSize());
		}

	}

	private Transformation getCachedResult(Alg alg) {
		return algMap.get(alg);
	}
}

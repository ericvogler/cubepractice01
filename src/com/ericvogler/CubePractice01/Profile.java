package com.ericvogler.CubePractice01;

public class Profile {
	static long lastTime;

	public static void check() {
		long time = System.currentTimeMillis();
		System.out.print("profiler: ");
		System.out.println(time - lastTime);
		lastTime = time;
	}
}

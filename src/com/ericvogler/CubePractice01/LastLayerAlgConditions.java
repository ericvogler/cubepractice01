package com.ericvogler.CubePractice01;

import java.util.ArrayList;
import java.util.HashMap;

public class LastLayerAlgConditions implements LastLayerConditions {
	private Alg[] ollAlgs = { //new Alg(""), 
			new Alg("F R U R' U' F'"),
			new Alg("F U R U' R' F'"), new Alg("R U R' U' R U2 R'"),
			new Alg("R' U'R U R' U2 R"), new Alg("R U2 R' U' R U' R'"),
			new Alg("R U R' U' R' F R F'"), new Alg("F R' F' R U R U' R'"),
			new Alg("R' U' R' F R F' U R"), };
	private Alg[] pllAlgs = {// new Alg(""),
			new Alg("R U R' U' R' F R2 U' R' U' R U R' F'"),
			new Alg("[R U'] [R U] [R U] [R U'] R' U' R2"),
			new Alg("R2 U [R U R' U'] (R' U') (R' U R')"),
			new Alg("[R U R' F'] {[R U R' U'] [R' F] [R2 U' R'] U'}"),
			//new Alg("F R U' R' U' [R U R' F'] {[R U R' U'] [R' F R F']}"), 
			};
	private Alg[] uTurns = { new Alg(""), new Alg("U"), new Alg("U2"),
			new Alg("U'") };
	private Alg[] blank = { new Alg("") };
	private HashMap<Cube, ArrayList<Alg>> algTable;

	public LastLayerAlgConditions() {
		makeAlgTable();
	}

	private void makeAlgTable() {
		HashMap<Cube, ArrayList<Alg>> algTable = new HashMap<Cube, ArrayList<Alg>>();
		for (Alg u1 : blank) {
			for (Alg a1 : ollAlgs) {
				for (Alg u2 : blank) {
					for (Alg a2 : pllAlgs) {
						for (Alg u3 : uTurns) {
							Alg solution = new Alg(new Alg[] { u1, a1, u2, a2,
									u3 });
							solution = Cancel.cancelledAlg(solution);
							Cube cube = new Cube(solution.getReverse());
							ArrayList<Alg> a = (algTable.containsKey(cube)) ? algTable
									.get(cube) : new ArrayList<Alg>();
							a.add(solution);
							algTable.put(cube, a);
						}
					}
				}
			}
		}
		this.algTable = algTable;
	}

	@Override
	public boolean isSatisfactory(Cube cube) {
		if (algTable.containsKey(cube)) {
			return true;
		} else {
			return false;
		}
	}

	public ArrayList<Alg> getAlgs(Cube cube) {
		if (algTable.containsKey(cube)) {
			return algTable.get(cube);
		} else {
			return null;
		}
	}

	public static void main(String[] args) {
		LastLayerAlgConditions llac = new LastLayerAlgConditions();
		System.out.println(llac.algTable.size());

		Cube cube = new Cube(new Alg("U F R U R' U' F'"));
		System.out.println(llac.isSatisfactory(cube));
		ArrayList<Alg> a = llac.getAlgs(cube);
		System.out.println(a.get(0));
	}

}

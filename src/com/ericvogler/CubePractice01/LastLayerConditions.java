package com.ericvogler.CubePractice01;

import java.util.ArrayList;

public interface LastLayerConditions {

	public boolean isSatisfactory(Cube cube);

	public ArrayList<Alg> getAlgs(Cube cube);
}
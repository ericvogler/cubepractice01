package com.ericvogler.CubePractice01;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;

public class PossibleNextF2LSteps {
	private Map<F2lPairWithLocation,HashSet<AlgWithSlotEffects>> allDoublesTable;

	/*
	 * Constructor: take array of F2L moves. Make table of what they solve and
	 * what they disrupt.
	 * 
	 * main method: public Alg[] getPossibleMoves(Cube cube);
	 */

	public PossibleNextF2LSteps(Alg[] f2lMoves) {
		makeMoveTable(f2lMoves);

	}

	public static ArrayList<F2lPair> unsolvedSlots(Cube cube) {
		ArrayList<F2lPair> result = new ArrayList<F2lPair>();
		for (F2lPair pair : F2lPair.values()) {
			if (!(cube.pieceIsSolved(pair.edge) && cube
					.pieceIsSolved(pair.corner))) {
				result.add(pair);
			}
		}
		return result;
	}

	private void makeMoveTable(Alg[] f2lMoves) {
		Cube cube;
		allDoublesTable = new HashMap();
		for (Alg alg : f2lMoves) {
			cube = new Cube();
			cube.transform(alg.getReverse());
			ArrayList<F2lPair> affectedSlots = unsolvedSlots(cube);
			for (F2lPair slot : affectedSlots) {
				F2lPairWithLocation p = new F2lPairWithLocation(slot,
						new PairLocation(cube, slot));
				AlgWithSlotEffects a = new AlgWithSlotEffects(alg,
						affectedSlots);
				if (!allDoublesTable.containsKey(p)) {
					HashSet<AlgWithSlotEffects> h = new HashSet<AlgWithSlotEffects>();
					h.add(a);
					allDoublesTable.put(p, h);
				} else {
					HashSet<AlgWithSlotEffects> h = (HashSet<AlgWithSlotEffects>) allDoublesTable
							.get(p);
					h.add(a);
				}
			}
		}
	}

	public ArrayList<Alg> getPossibilities(Cube cube) {
		HashSet<AlgWithSlotEffects> algs = getAllCandidateAlgs(cube);
		return nondestructiveAlgs(algs, unsolvedSlots(cube));
	}

	private HashSet<AlgWithSlotEffects> getAllCandidateAlgs(Cube cube) {
		HashSet<AlgWithSlotEffects> result = new HashSet<AlgWithSlotEffects>();
		ArrayList<F2lPair> unsolvedSlots = unsolvedSlots(cube);
		for (F2lPair slot : unsolvedSlots) {
			F2lPairWithLocation p = new F2lPairWithLocation(slot,
					new PairLocation(cube, slot));
			HashSet<AlgWithSlotEffects> h = (HashSet<AlgWithSlotEffects>) allDoublesTable
					.get(p);
			if (h != null) {
				for (AlgWithSlotEffects a : h) {
					result.add(a);
				}
			}
		}
		return result;
	}

	private ArrayList<Alg> nondestructiveAlgs(
			Iterable<AlgWithSlotEffects> algs, ArrayList<F2lPair> unsolvedSlots) {
		ArrayList<Alg> result = new ArrayList<Alg>();
		for (AlgWithSlotEffects alg : algs) {
			if (unsolvedSlots.containsAll(alg.getSlots())) {
				result.add(alg.getAlg());
			}
		}
		return result;
	}

	private Alg shortesAlg(ArrayList<Alg> algs) {
		int bestScore = 9999;
		Alg bestAlg = new Alg("R");
		for (int i = 0; i < algs.size(); i++) {
			if (algs.get(i).getLength() < bestScore) {
				bestAlg = algs.get(i);
				bestScore = algs.get(i).getLength();
			}
		}
		return bestAlg;
	}

		
	public static Alg randomAlg(ArrayList<Alg> algs) {
		return algs.get((int) (Math.random() * algs.size()));
	}

	public static void main(String[] args) {
		Alg scramble = new Scrambler().getScramble();
		System.out.println(scramble);
		
		PossibleNextF2LSteps pns = new PossibleNextF2LSteps(
				F2lMoves.allDoubleAlgs());
		Cube cube = new Cube(scramble);
		ArrayList<Alg> solution = pns.randomSolution(cube);
		int moveCount = 0;
		for (Alg a : solution) {
			System.out.println(a);
			moveCount += a.getLength();
		}
		System.out.println(moveCount);

	}
}

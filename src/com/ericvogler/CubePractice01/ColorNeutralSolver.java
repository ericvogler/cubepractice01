package com.ericvogler.CubePractice01;

import java.util.ArrayList;

public class ColorNeutralSolver {
	public static void main(String[] args) {
		String[] starts = new String[] { "", "x2", "z", "z'", "x", "x'", };
		String[] ends = new String[] { "", "x2", "z'", "z", "x'", "x", };
		// Alg scramble = new Scrambler().getScramble();
		Alg scramble = new Alg(
				"  L2 B2 R2 U' L2 U2 R2 D B2 U L' B' F2 R D B U' B2 L R'  ");
		System.out.println(scramble);
		Alg[] scrambles = new Alg[6];
		for (int i = 0; i < 6; i++) {
			scrambles[i] = new Alg(starts[i] + " " + scramble.toString() + " " + ends[i]);
		}

		int bestScore = 99;
		Alg bestAlg = new Alg("");
		Alg bestScramble = new Alg("");
		String bestInspection = "-";
		Solver solver;
		Profile.check();
		for (int i = 0; i < 6; i++) {
			System.out.println(ends[i]);
			 LastLayerConditions lastLayerConditions = new LastLayerBasicConditions(false, false,
					 false, false, false);
//			 LastLayerConditions lastLayerConditions = new LastLayerAlgConditions();
			solver = new Solver(lastLayerConditions, new String[] { "R U R'", "R U2 R'", "R'FRF'"
					}, true, true);

			solver.setScramble(scrambles[i]);
			solver.setSubOptimalCrossDegree(0);
			solver.setAllowXCross(false);
			solver.setBestScore(bestScore);
			Alg result = solver.solve();

			if (result != null) {
				System.out.println(solver.getBestScore());
				bestScore = solver.getBestScore();
				bestInspection = ends[i];
				bestAlg = result;
				bestScramble = scrambles[i];
			}
		}
		Profile.check();
		System.out.println(scramble);
		System.out.println(bestScore);
		System.out.println(bestInspection);
		System.out.println(PrettySolution.solutionInChunks(bestScramble, bestAlg));

	}
}

package com.ericvogler.CubePractice01;

import java.util.Arrays;
import java.util.HashMap;


public class MoveCountCounter {
	
	private HashMap counter;

	
	public MoveCountCounter() {
		this.counter = new HashMap();
	}
	
	public void count(Object n) {
		if (counter.containsKey(n)) {
			int count = (int)counter.get(n);
			counter.put(n, count + 1);
		} else {
			counter.put(n, 1);
		}
	}
	
	@Override
	public String toString() {
		String result = "";
		Object[] keyArray = counter.keySet().toArray();
		Arrays.sort(keyArray);
		for (Object o: keyArray) {
			result = result + o.toString() + ":" + counter.get(o).toString() + " ";
		}
		return result;
	}
	
	public static void main(String[] args) {
		MoveCountCounter mc = new MoveCountCounter();
		mc.count(1);
		mc.count(2);
		mc.count(2);
		mc.count(2);
		mc.count(2);
		//mc.count("E");
		System.out.println(mc);
	
	}
}

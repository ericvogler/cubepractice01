package com.ericvogler.CubePractice01;

import java.util.ArrayList;

public enum Corner implements Piece {
	UFR, URB, UBL, ULF, DRF, DFL, DLB, DBR;

	public static Corner[] corners(Piece[] pieces) {
		ArrayList<Corner> result = new ArrayList<Corner>();
		for (Piece piece: pieces) {
			if (piece instanceof Corner) {
				result.add((Corner)piece);
			}
		}
		Corner[] resultArray = new Corner[result.size()];
		for (int i = 0; i<resultArray.length; i++) {
			resultArray[i] = result.get(i);
		}
		return resultArray;
	}

}

package com.ericvogler.CubePractice01;

public class Transformation {
	public byte[] edgePermutation;
	public byte[] cornerPermutation;
	public byte[] edgeOrientation;
	public byte[] cornerOrientation;
	public byte[] centerOrientation;
	

	public Transformation(byte[] edgePermutation, byte[] cornerPermutation,
			byte[] edgeOrientation, byte[] cornerOrientation,
			byte[] centerOrientation) {
		this.edgePermutation = edgePermutation;
		//XXX:Am I a bad person? Keeping arrays indexed 12-20 for corners because I'm lazy?
		this.cornerPermutation = cornerPermutation;
		for (int i=0; i<cornerPermutation.length; i++) {
			this.cornerPermutation[i] = (byte) (this.cornerPermutation[i] %12); 
		}
		this.edgeOrientation = edgeOrientation;
		this.cornerOrientation = cornerOrientation;
		this.centerOrientation = centerOrientation;
	}
}

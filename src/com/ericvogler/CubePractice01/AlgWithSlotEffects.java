package com.ericvogler.CubePractice01;

import java.util.ArrayList;

public class AlgWithSlotEffects {

	private ArrayList<F2lPair> pairs;
	private Alg alg;

	
	public ArrayList<F2lPair> getSlots() {
		return pairs;
	}

	public Alg getAlg() {
		return alg;
	}

	public AlgWithSlotEffects(Alg alg, ArrayList<F2lPair> pairs) {
		this.pairs = pairs;
		this.alg = alg;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((alg == null) ? 0 : alg.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AlgWithSlotEffects other = (AlgWithSlotEffects) obj;
		if (alg == null) {
			if (other.alg != null)
				return false;
		} else if (!alg.equals(other.alg))
			return false;
		return true;
	}
	
}

package com.ericvogler.CubePractice01;

public class PrettySolution {

	private static int degreeOfSolvedness(Cube cube) {
		if (!cube.piecesAreSolved(new Piece[] { Edge.DR, Edge.DL, Edge.DB,
				Edge.DF })) {
			return 0;
		}
		return 5 - PossibleNextF2LSteps.unsolvedSlots(cube).size();
	}

	public static String solutionInChunks(Alg scramble, Alg solution) {
		StringBuilder sb = new StringBuilder();
		Cube cube = new Cube(scramble);
		int degrees = degreeOfSolvedness(cube);
		int maxSoFar = degrees;
		for (Move move : solution.getMoves()) {
			sb.append(move.toString().replaceAll("p", "'"));
			sb.append(" ");
			cube.transform(move);
			degrees = degreeOfSolvedness(cube);
			if (degrees > maxSoFar) {
				maxSoFar = degrees;
				sb.append("\n");
			}
		}
		int cutoff = (sb.substring(sb.length() - 2, sb.length()) == " \n") ? 2
				: 1;
		return sb.substring(0, sb.length() - cutoff);
	}

	public static void main(String[] args) {
		Alg scramble = new Alg(
				"R F U F2 U2 L U B' U' R' D B2 U2 R B L2 F2 U B D2 L' U2 B' U' L2");
		Alg solution = new Alg(
				"F2 R' D2 F D B L' U2 L U' L U' L' B' U' B2 U B' U2 L U L' F' U F");
		String s = solutionInChunks(scramble, solution);
		System.out.println(s);
	}
}

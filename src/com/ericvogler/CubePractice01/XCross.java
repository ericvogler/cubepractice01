/*
 * Concept of XCross: Return either:
 * -optimal or suboptimal crosses
 * -optimal or suboptimal crosses with xcrosses allowed
 * -optimal or suboptimal xcrosses only.
 * 
 * Steps:
 * -figure out optimal movecount
 * -return appropriate count of only crosses/all crosses/only xcrosses
 */
package com.ericvogler.CubePractice01;

import java.util.ArrayList;

public class XCross {

	public static ArrayList<Alg> getCrosses(Cube cube, int nonOptimal) {
		Piece[] cross = { Edge.DF, Edge.DB, Edge.DL, Edge.DR };
		BlockSolver bs = new BlockSolver(cube, cross);
		ArrayList<Alg> crosses = bs.getSolutions();
		int optimal = crosses.get(0).getLength();
		int moveCount = (optimal + nonOptimal <= 10) ? optimal + nonOptimal : 10;
		if (crosses.get(0).getLength() < moveCount) {
			crosses = bs.getSolutions(moveCount);
		}
		return crosses;
	}

	public static ArrayList<Alg> getCrossesNoX(Cube cube, int nonOptimal) {
		ArrayList<Alg> crosses = getCrosses(cube, nonOptimal);
		ArrayList<Alg> result = new ArrayList<Alg>();
		for (Alg alg : crosses) {
			Cube cube2 = new Cube(cube);
			cube2.transform(alg);
			if (PossibleNextF2LSteps.unsolvedSlots(cube2).size() == 4) {
				result.add(alg);
			}
		}
		return result;
	}

	public static void main(String[] args) {
		Cube cube = new Cube(new Scrambler().getScramble());
		ArrayList<Alg> c = getCrosses(cube, 0);
		System.out.println(c.get(0));
	}

}

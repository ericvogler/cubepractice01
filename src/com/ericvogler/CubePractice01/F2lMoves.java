package com.ericvogler.CubePractice01;

import java.util.HashSet;

public class F2lMoves {
	private static String[] baseSequences = { "R U R'", "R U2 R'",
	// "F R' F' R",
	// "R2 F2 R2 F2",
	// "R2 U2 R2 U2 R2",
	// "F' B U B F'",
	// "B D L2 D' B'",
	// "B' R' U2 R B",
	// "F R' U' R F'",
	// "B R B2 R' B'",
	// "R D B D' R'",
	// "L B2 L B2 L'",
	// "L2 U' L' U L2",
	// "R L U' L' R'",
	// "B U L2 B' L2",
	// "F2 L2 F L2 F",
	// "F2 D' L2 D F2",
	// "L2 D F D' L2",
	// "R2 U2 R' U2 R2",
	// "B L U L' B'",
	// "F2 D2 B' D2 F2",
	// "L B U2 B' L'",
	// "R2 B' R2 U2 B",
	// "L B2 L2 B2 L'",
	// "L' U B2 L B2",
	// "R' F U2 R F'",
	// "F2 D2 B2 D2 F2",
	// "R' B' R B R",
	// "L' D2 R' D2 L",
	// "B' L' U L B",
	// "R' L U2 L' R",
	// "L' B U B' L",
	// "B2 R B R' B",
	// "B2 U' B2 U B2",
	// "B D2 F2 D2 B'",
	// "R' L U' L' R",
	// "R' L' U2 R L",
	// "R' F U' F' R",
	// "R' F U2 F' R",
	};

	private static Alg rotateAlg(Alg alg, int qTurns) {
		if (qTurns == 0)
			return alg;
		Move[] moves = alg.getMoves();
		for (int i = 0; i < moves.length; i++) {
			moves[i] = rotateMove(moves[i], qTurns);
		}
		return new Alg(moves);
	}

	private static Alg mirrorAlg(Alg alg) {
		Move[] moves = alg.getMoves();
		for (int i = 0; i < moves.length; i++) {
			moves[i] = mirrorMove(moves[i]);
		}
		return new Alg(moves);
	}

	private static Move rotateMove(Move move, int qTurns) {
		switch (qTurns) {
		case (0):
			return move;
		case (1):
			switch (move) {
			case Lp:
				return Move.Bp;
			case F2:
				return Move.L2;
			case B:
				return Move.R;
			case D:
				return Move.D;
			case R2:
				return Move.F2;
			case F:
				return Move.L;
			case Dp:
				return Move.Dp;
			case L:
				return Move.B;
			case U2:
				return Move.U2;
			case Bp:
				return Move.Rp;
			case Rp:
				return Move.Fp;
			case R:
				return Move.F;
			case U:
				return Move.U;
			case B2:
				return Move.R2;
			case Fp:
				return Move.Lp;
			case L2:
				return Move.B2;
			case Up:
				return Move.Up;
			case D2:
				return Move.D2;
			}
		case (2):
			switch (move) {
			case Lp:
				return Move.Rp;
			case F2:
				return Move.B2;
			case B:
				return Move.F;
			case D:
				return Move.D;
			case R2:
				return Move.L2;
			case F:
				return Move.B;
			case Dp:
				return Move.Dp;
			case L:
				return Move.R;
			case U2:
				return Move.U2;
			case Bp:
				return Move.Fp;
			case Rp:
				return Move.Lp;
			case R:
				return Move.L;
			case U:
				return Move.U;
			case B2:
				return Move.F2;
			case Fp:
				return Move.Bp;
			case L2:
				return Move.R2;
			case Up:
				return Move.Up;
			case D2:
				return Move.D2;
			}
		case (3):
			switch (move) {
			case Lp:
				return Move.Fp;
			case F2:
				return Move.R2;
			case B:
				return Move.L;
			case D:
				return Move.D;
			case R2:
				return Move.B2;
			case F:
				return Move.R;
			case Dp:
				return Move.Dp;
			case L:
				return Move.F;
			case U2:
				return Move.U2;
			case Bp:
				return Move.Lp;
			case Rp:
				return Move.Bp;
			case R:
				return Move.B;
			case U:
				return Move.U;
			case B2:
				return Move.L2;
			case Fp:
				return Move.Rp;
			case L2:
				return Move.F2;
			case Up:
				return Move.Up;
			case D2:
				return Move.D2;
			}
		}
		return move;
	}

	private static Move mirrorMove(Move move) {
		switch (move) {
		case Lp:
			return Move.R;
		case F2:
			return Move.F2;
		case B:
			return Move.Bp;
		case D:
			return Move.Dp;
		case R2:
			return Move.L2;
		case F:
			return Move.Fp;
		case Dp:
			return Move.D;
		case L:
			return Move.Rp;
		case U2:
			return Move.U2;
		case Bp:
			return Move.B;
		case Rp:
			return Move.L;
		case R:
			return Move.Lp;
		case U:
			return Move.Up;
		case B2:
			return Move.B2;
		case Fp:
			return Move.F;
		case L2:
			return Move.R2;
		case Up:
			return Move.U;
		case D2:
			return Move.D2;
		}
		return move;
	}

	public static Alg[] allRotationsMirrorsAndInversions(Alg alg) {
		Alg[] result = new Alg[16];
		int resultPos = 0;
		for (int mirror = 0; mirror < 2; mirror++)
			for (int invert = 0; invert < 2; invert++)
				for (int rotations = 0; rotations < 4; rotations++) {
					Alg tempAlg = new Alg(alg);
					if (mirror == 1)
						tempAlg = mirrorAlg(tempAlg);
					if (invert == 1)
						tempAlg.reverse();
					if (rotations > 0)
						tempAlg = rotateAlg(tempAlg, rotations);
					result[resultPos++] = tempAlg;
				}
		return result;
	}

	public static void addDMoves(HashSet<Alg> algs) {
		HashSet<Alg> dAlgs = new HashSet<Alg>();
		for (Alg dAlg : new Alg[] { new Alg("D"), new Alg("D2"), new Alg("D'") }) {
			for (Alg alg : algs) {
				dAlgs.add(new Alg(new Alg[] { dAlg, alg, dAlg.getReverse() }));
			}
		}
		for (Alg alg : dAlgs) {
			algs.add(alg);
		}
	}

	public static Alg[] nonStandardF2Ls(int moveCount) {
		Transformations.getInstance().setCacheResults(false);
		HashSet<Alg> algs = new HashSet<Alg>();
		MovesIterator mi = new MovesIterator(moveCount);
		Cube cube;
		Alg alg;
		Piece[] cross = new Piece[] { Edge.DF, Edge.DB, Edge.DL, Edge.DR };
		do {
			alg = new Alg(mi.getNext());
			cube = new Cube(alg);
			if (cube.piecesAreSolved(cross)) {
				algs.add(new Alg(alg));
				if (algs.size() % 1000 == 0) {
					System.out.println(algs.size());
				}
			}
		} while (!mi.isFinished());
		return hashSetToAlgs(algs);
	}

	private static Alg[] hashSetToAlgs(HashSet<Alg> algs) {
		Alg[] result = new Alg[algs.size()];
		int algPos = 0;
		for (Object a : algs.toArray())
			result[algPos++] = (Alg) a;
		return result;
	}

	public static Alg[] allSingleAlgs() {
		return allSingleAlgs(F2lMoves.baseSequences, false);
	}

	public static Alg[] allSingleAlgs(String baseSequences[], boolean dMoves) {
		Alg[] uMoves = { null, new Alg("U"), new Alg("U2"), new Alg("U'") };
		HashSet<Alg> algs = new HashSet<Alg>();
		for (String base : baseSequences) {
			for (Alg a : allRotationsMirrorsAndInversions(new Alg(base)))
				for (Alg u : uMoves) {
					Alg temp;
					if (u == null)
						temp = a;
					else
						temp = new Alg(new Alg[] { u, a });
					algs.add(temp);
				}
		}
		if (dMoves) {
			addDMoves(algs);
		}
		Alg[] result = hashSetToAlgs(algs);
		return result;
	}

	public static Alg[] allDoubleAlgs() {
		return allDoubleAlgs(F2lMoves.baseSequences, true, false);
	}

	public static Alg[] allDoubleAlgs(String[] baseSequences, boolean cancel,
			boolean dMoves) {
		HashSet<Alg> algs = new HashSet<Alg>();
		Alg[] singles = allSingleAlgs(baseSequences, dMoves);
		for (Alg a1 : singles) {
			algs.add(a1); // include singles
			for (Alg a2 : singles) {
				Alg[] a = { a1, a2 };
				Alg temp = new Alg(a);
				if (cancel) {
					temp = Cancel.cancelledAlg(temp);
				}
				algs.add(temp);
			}
		}
		return hashSetToAlgs(algs);
	}

	@SuppressWarnings("unused")
	public static Alg[] allTripleAlgs() {
		// LOL
		HashSet<Alg> algs = new HashSet<Alg>();
		for (Alg a1 : allSingleAlgs())
			for (Alg a2 : allSingleAlgs())
				for (Alg a3 : allSingleAlgs()) {
					Alg[] a = { a1, a2, a3 };
					Alg temp = new Alg(a);
					algs.add(temp);
				}
		return hashSetToAlgs(algs);
	}

	public static void main(String[] args) {
		Alg[] singles = allSingleAlgs(new String[] {"R U2 R'"}, false);
		for (Alg alg: singles) {
			System.out.println(alg);
		}
	}

}

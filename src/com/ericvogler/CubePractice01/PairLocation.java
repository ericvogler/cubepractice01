package com.ericvogler.CubePractice01;

import java.util.Arrays;

public class PairLocation {
	public EdgeLocation edgeLocation;
	public CornerLocation cornerLocation;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cornerLocation == null) ? 0 : cornerLocation.hashCode());
		result = prime * result
				+ ((edgeLocation == null) ? 0 : edgeLocation.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PairLocation other = (PairLocation) obj;
		if (cornerLocation == null) {
			if (other.cornerLocation != null)
				return false;
		} else if (!cornerLocation.equals(other.cornerLocation))
			return false;
		if (edgeLocation == null) {
			if (other.edgeLocation != null)
				return false;
		} else if (!edgeLocation.equals(other.edgeLocation))
			return false;
		return true;
	}

	//XXX: Multiple constructors here maybe over the top? Or should funnel into one main constructor?
	
	public PairLocation(Cube cube, Edge edge, Corner corner) {
		this.edgeLocation = cube.getEdgeLocation(edge);
		this.cornerLocation = cube.getCornerLocation(corner);
	}

	public PairLocation(Cube cube, F2lPair f2lPair) {
		this.edgeLocation = cube.getEdgeLocation(f2lPair.edge);
		this.cornerLocation = cube.getCornerLocation(f2lPair.corner);
	}

	public PairLocation(EdgeLocation edgeLocation, CornerLocation cornerLocation) {
		this.edgeLocation = edgeLocation;
		this.cornerLocation = cornerLocation;
	}
	
	
	public static void main(String[] args) {
		Cube cube = new Cube();
		cube.transform(new Alg("R U R' U'"));
		PairLocation pl = new PairLocation(cube, Edge.UB, Corner.DRF);
		cube.transform("R");
		PairLocation pl2 = new PairLocation(cube, Edge.UB, Corner.DRF);
		System.out.println(pl.equals(pl2));
	}
}

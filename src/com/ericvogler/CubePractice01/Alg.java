package com.ericvogler.CubePractice01;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class Alg {
	// "R":Moves.R, etc...:
	private static final Map<String, Move> ltnMap;
	static {
		Map<String, Move> tempMap = new HashMap<String, Move>();
		tempMap.put("R", Move.R);
		tempMap.put("R'", Move.Rp);
		tempMap.put("R2", Move.R2);
		tempMap.put("L", Move.L);
		tempMap.put("L'", Move.Lp);
		tempMap.put("L2", Move.L2);
		tempMap.put("U", Move.U);
		tempMap.put("U'", Move.Up);
		tempMap.put("U2", Move.U2);
		tempMap.put("D", Move.D);
		tempMap.put("D'", Move.Dp);
		tempMap.put("D2", Move.D2);
		tempMap.put("F", Move.F);
		tempMap.put("F'", Move.Fp);
		tempMap.put("F2", Move.F2);
		tempMap.put("B", Move.B);
		tempMap.put("B'", Move.Bp);
		tempMap.put("B2", Move.B2);
		tempMap.put("r", Move.Rw);
		tempMap.put("r'", Move.Rwp);
		tempMap.put("r2", Move.Rw2);
		tempMap.put("l", Move.Lw);
		tempMap.put("l'", Move.Lwp);
		tempMap.put("l2", Move.Lw2);
		tempMap.put("u", Move.Uw);
		tempMap.put("u'", Move.Uwp);
		tempMap.put("u2", Move.Uw2);
		tempMap.put("d", Move.Dw);
		tempMap.put("d'", Move.Dwp);
		tempMap.put("d2", Move.Dw2);
		tempMap.put("f", Move.Fw);
		tempMap.put("f'", Move.Fwp);
		tempMap.put("f2", Move.Fw2);
		tempMap.put("b", Move.Bw);
		tempMap.put("b'", Move.Bwp);
		tempMap.put("b2", Move.Bw2);
		tempMap.put("x", Move.x);
		tempMap.put("x'", Move.xp);
		tempMap.put("x2", Move.x2);
		tempMap.put("y", Move.y);
		tempMap.put("y'", Move.yp);
		tempMap.put("y2", Move.y2);
		tempMap.put("z", Move.z);
		tempMap.put("z'", Move.zp);
		tempMap.put("z2", Move.z2);
		tempMap.put("e", Move.e);
		tempMap.put("e'", Move.ep);
		tempMap.put("e2", Move.e2);
		tempMap.put("s", Move.s);
		tempMap.put("s'", Move.sp);
		tempMap.put("s2", Move.s2);
		tempMap.put("m", Move.m);
		tempMap.put("m'", Move.mp);
		tempMap.put("m2", Move.m2);

		ltnMap = Collections.unmodifiableMap(tempMap);
	}
	private static final Map<Move, String> ntlMap;
	static {
		Map<Move, String> tempMap = new HashMap<Move, String>();
		tempMap.put(Move.R, "R");
		tempMap.put(Move.Rp, "R'");
		tempMap.put(Move.R2, "R2");
		tempMap.put(Move.L, "L");
		tempMap.put(Move.Lp, "L'");
		tempMap.put(Move.L2, "L2");
		tempMap.put(Move.U, "U");
		tempMap.put(Move.Up, "U'");
		tempMap.put(Move.U2, "U2");
		tempMap.put(Move.D, "D");
		tempMap.put(Move.Dp, "D'");
		tempMap.put(Move.D2, "D2");
		tempMap.put(Move.F, "F");
		tempMap.put(Move.Fp, "F'");
		tempMap.put(Move.F2, "F2");
		tempMap.put(Move.B, "B");
		tempMap.put(Move.Bp, "B'");
		tempMap.put(Move.B2, "B2");
		tempMap.put(Move.Rw, "r");
		tempMap.put(Move.Rwp, "r'");
		tempMap.put(Move.Rw2, "r2");
		tempMap.put(Move.Lw, "l");
		tempMap.put(Move.Lwp, "l'");
		tempMap.put(Move.Lw2, "l2");
		tempMap.put(Move.Uw, "u");
		tempMap.put(Move.Uwp, "u'");
		tempMap.put(Move.Uw2, "u2");
		tempMap.put(Move.Dw, "d");
		tempMap.put(Move.Dwp, "d'");
		tempMap.put(Move.Dw2, "d2");
		tempMap.put(Move.Fw, "f");
		tempMap.put(Move.Fwp, "f'");
		tempMap.put(Move.Fw2, "f2");
		tempMap.put(Move.Bw, "b");
		tempMap.put(Move.Bwp, "b'");
		tempMap.put(Move.Bw2, "b2");
		tempMap.put(Move.x, "x");
		tempMap.put(Move.xp, "x'");
		tempMap.put(Move.x2, "x2");
		tempMap.put(Move.y, "y");
		tempMap.put(Move.yp, "y'");
		tempMap.put(Move.y2, "y2");
		tempMap.put(Move.z, "z");
		tempMap.put(Move.zp, "z'");
		tempMap.put(Move.z2, "z2");
		tempMap.put(Move.e, "e");
		tempMap.put(Move.ep, "e'");
		tempMap.put(Move.e2, "e2");
		tempMap.put(Move.s, "s");
		tempMap.put(Move.sp, "s'");
		tempMap.put(Move.s2, "s2");
		tempMap.put(Move.m, "m");
		tempMap.put(Move.mp, "m'");
		tempMap.put(Move.m2, "m2");
		ntlMap = Collections.unmodifiableMap(tempMap);
	}
	private static final Map<Move, Move> reverseMap;
	static {
		Map<Move, Move> tempMap = new HashMap<Move, Move>();
		tempMap.put(Move.R, Move.Rp);
		tempMap.put(Move.Rp, Move.R);
		tempMap.put(Move.L, Move.Lp);
		tempMap.put(Move.Lp, Move.L);
		tempMap.put(Move.U, Move.Up);
		tempMap.put(Move.Up, Move.U);
		tempMap.put(Move.D, Move.Dp);
		tempMap.put(Move.Dp, Move.D);
		tempMap.put(Move.F, Move.Fp);
		tempMap.put(Move.Fp, Move.F);
		tempMap.put(Move.B, Move.Bp);
		tempMap.put(Move.Bp, Move.B);
		tempMap.put(Move.Rw, Move.Rwp);
		tempMap.put(Move.Rwp, Move.Rw);
		tempMap.put(Move.Lw, Move.Lwp);
		tempMap.put(Move.Lwp, Move.Lw);
		tempMap.put(Move.Uw, Move.Uwp);
		tempMap.put(Move.Uwp, Move.Uw);
		tempMap.put(Move.Dw, Move.Dwp);
		tempMap.put(Move.Dwp, Move.Dw);
		tempMap.put(Move.Fw, Move.Fwp);
		tempMap.put(Move.Fwp, Move.Fw);
		tempMap.put(Move.Bw, Move.Bwp);
		tempMap.put(Move.Bwp, Move.Bw);
		tempMap.put(Move.s, Move.sp);
		tempMap.put(Move.sp, Move.s);
		tempMap.put(Move.e, Move.ep);
		tempMap.put(Move.ep, Move.e);
		tempMap.put(Move.m, Move.mp);
		tempMap.put(Move.mp, Move.m);
		tempMap.put(Move.x, Move.xp);
		tempMap.put(Move.xp, Move.x);
		tempMap.put(Move.y, Move.yp);
		tempMap.put(Move.yp, Move.y);
		tempMap.put(Move.z, Move.zp);
		tempMap.put(Move.zp, Move.z);
		reverseMap = Collections.unmodifiableMap(tempMap);
	}
	private Move[] algValue;

	public Alg(String notation) {
		Move[] a = notationToMoves(notation);
		this.algValue = a;
	}

	public Alg(Move[] algValue) {
		this.algValue = algValue;
	}

	public Alg(Alg alg) {
		Move[] g = alg.getMoves();
		this.algValue = Arrays.copyOf(g, g.length);
	}

	public Alg(Alg[] algs) {
		this.algValue = combineAlgs(algs);
	}

	public Alg(ArrayList<Alg> algs) {
		Alg[] a = new Alg[algs.size()];
		algs.toArray(a);
		this.algValue = combineAlgs(a);
	}

	private Move[] combineAlgs(Alg[] algs) {
		int length = 0;
		for (Alg a : algs)
			length += a.getLength();
		Move[] combined = new Move[length];
		int pos = 0;
		for (Alg a : algs) {
			for (Move m : a.getMoves())
				combined[pos++] = m;
		}
		return combined;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(algValue);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Alg other = (Alg) obj;
		if (!Arrays.equals(algValue, other.algValue))
			return false;
		return true;
	}

	public Move[] getMoves() {
		return this.algValue;
	}

	public String getNotation() {
		String notation = movesToNotation(this.algValue);
		return notation;
	}

	public int getLength() {
		return this.algValue.length;
	}

	@Override
	public String toString() {
		return getNotation();
	}

	private Move[] notationToMoves(String notatedAlg) {
		ArrayList<String> moveList = separateMovesFromString(notatedAlg);
		Move[] moves = new Move[moveList.size()];
		for (int i = 0; i < moveList.size(); i++) {
			moves[i] = letterToNumber(moveList.get(i));
		}
		return moves;
	}

	private static String concatWithSpaces(String[] words) {
		StringBuilder wordList = new StringBuilder();
		for (String word : words) {
			wordList.append(word + " ");
		}
		return new String(wordList.deleteCharAt(wordList.length() - 1));
	}

	private String movesToNotation(Move[] moves) {
		String[] notatedMoves = new String[moves.length];
		for (int i = 0; i < moves.length; i++) {
			notatedMoves[i] = ntlMap.get(moves[i]);
		}
		String notation = concatWithSpaces(notatedMoves);
		return notation;
	}

	private static Move letterToNumber(String letter) {
		return ltnMap.get(letter);
	}

	private static ArrayList<String> separateMovesFromString(String notatedAlg) {
		String myPattern = "[rludfbRLUDFByxzesm][2']?";
		Pattern pattern = Pattern.compile(myPattern);
		Matcher matcher = pattern.matcher(notatedAlg);
		ArrayList<String> moves = new ArrayList<String>();
		while (matcher.find())
			moves.add(matcher.group());
		return moves;
	}

	public void reverse() {
		Move[] result = getReverseValue();
		algValue = result;
	}

	public Alg getReverse() {
		return new Alg(getReverseValue());
	}

	private Move[] getReverseValue() {
		Move[] result = new Move[algValue.length];
		for (int i = 0; i < result.length; i++) {
			Move nextVal = algValue[algValue.length - i - 1];
			nextVal = nextVal.getReverse();
			result[i] = nextVal;
		}
		return result;
	}

	public int HTM() {
		return algValue.length;
	}

	public static void main(String[] args) {
		Alg alg = new Alg("R U R' U' L2");
		System.out.println(alg.getNotation());
		alg.reverse();
		System.out.println(alg.getNotation());
	}
}
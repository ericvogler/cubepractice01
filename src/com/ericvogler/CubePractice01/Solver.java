package com.ericvogler.CubePractice01;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.awt.Toolkit;

public class Solver implements Runnable {
	private PossibleNextF2LSteps possibleDoubles;
	private int bestScore = 9999;
	private Alg bestAlg;
	private int crossesSearched = 0;
	private int nodesSearched = 0;
	private int totalCrosses = -1;
	private int solutionsSearched = 0;
	private boolean finished = false;
	private Alg scramble;
	private LastLayerConditions lastLayerConditions;
	private boolean cancelMoves = true;
	private int subOptimalCrossDegree = 0;
	private boolean killMe = false;
	private boolean allowXCross;
	private PossibleNextF2LSteps possibleSingles;

	public int getBestScore() {
		return bestScore;
	}

	public void setBestScore(int bestScore) {
		this.bestScore = bestScore;
	}

	public Alg getBestAlg() {
		return bestAlg;
	}

	public int getCrossesSearched() {
		return crossesSearched;
	}

	public int getNodesSearched() {
		return this.nodesSearched;
	}

	public int getTotalCrosses() {
		return totalCrosses;
	}

	public int getSolutionsSearched() {
		return solutionsSearched;
	}

	public boolean isFinished() {
		return finished;
	}

	public void setSubOptimalCrossDegree(int subOptimalCrossDegree) {
		this.subOptimalCrossDegree = subOptimalCrossDegree;
	}

	public void setScramble(Alg scramble) {
		this.scramble = scramble;
	}

	public void tryToKill() {
		this.killMe = true;
	}

	public Solver(LastLayerConditions lastLayerBasicConditions, String[] baseF2lSequences,
			boolean cancelMoves, boolean includeDMoves) {

		Alg[] f2lMoves = F2lMoves.allDoubleAlgs(baseF2lSequences, cancelMoves, includeDMoves);
		possibleSingles = new PossibleNextF2LSteps(F2lMoves.allSingleAlgs(baseF2lSequences,
				includeDMoves));
		possibleDoubles = new PossibleNextF2LSteps(f2lMoves);
		this.lastLayerConditions = lastLayerBasicConditions;
		this.cancelMoves = cancelMoves;
		this.killMe = false;
	}

	public Solver(LastLayerConditions lastLayerBasicConditions) {
		possibleSingles = new PossibleNextF2LSteps(F2lMoves.allSingleAlgs());
		possibleDoubles = new PossibleNextF2LSteps(F2lMoves.allDoubleAlgs());
		this.lastLayerConditions = lastLayerBasicConditions;
		this.killMe = false;
	}

	public Solver() {
		this(new LastLayerBasicConditions(false, false, false, false, false));
	}

	private static void sortAlgsByLength(ArrayList<Alg> algs) {
		Collections.sort(algs, new Comparator<Alg>() {
			public int compare(Alg a1, Alg a2) {
				int len1 = a1.getLength();
				int len2 = a2.getLength();
				if (len1 < len2)
					return -1;
				if (len1 == len2)
					return 0;
				return 1;
			}
		});
	}

	private void solutionReceiver(Alg solution) {
		this.solutionsSearched++;
		if (solution.getLength() < this.bestScore) {
			Cube testCube = new Cube(scramble, solution);
			if (!lastLayerConditions.isSatisfactory(testCube)
					|| !testCube
							.piecesAreSolved(new Piece[] { Edge.DR, Edge.DL, Edge.DF, Edge.DB })) {
				return;
			}
			this.bestScore = solution.getLength();
			this.bestAlg = new Alg(new Alg[] { solution,
					lastLayerConditions.getAlgs(testCube).get(0) });
			System.out.println(this.bestScore);
		}
	}

	private void solveASlot(Cube cube, ArrayList<Alg> partialSolution) {
		if (this.killMe) {
			return;
		}
		this.nodesSearched++;
		ArrayList<Alg> nextSteps = possibleSingles.getPossibilities(cube);
		if (nextSteps.size() == 0) {
			nextSteps = possibleDoubles.getPossibilities(cube);
		}
		sortAlgsByLength(nextSteps);
		for (Alg nextStep : nextSteps) {
			Cube nextCube = new Cube(cube);
			nextCube.transform(nextStep);
			ArrayList<Alg> partialSolutionPlusNext = (ArrayList<Alg>) partialSolution.clone();
			partialSolutionPlusNext.add(nextStep);
			Alg cancelledAlg = (this.cancelMoves) ? Cancel.cancelledAlg(new Alg(
					partialSolutionPlusNext)) : new Alg(partialSolutionPlusNext);
			int unsolvedSlots = PossibleNextF2LSteps.unsolvedSlots(nextCube).size();
			if (cancelledAlg.getLength() + (3 * unsolvedSlots) > this.bestScore) {
				continue;
			}
			if (unsolvedSlots == 0) {
				solutionReceiver(cancelledAlg);
			} else {
				solveASlot(nextCube, partialSolutionPlusNext);
			}
		}
	}

	public static void main(String[] args) {
		// LastLayerConditions solveConditon = new
		// LastLayerBasicConditions(false, true, false,
		// true, true);
		LastLayerConditions solveCondition = new LastLayerAlgConditions();
		Solver solver = new Solver(solveCondition, new String[] { "R U R'", "R U2 R'" }, true,
				false);
		// Alg scramble2 = new Scrambler().getScramble();
		// System.out.println(scramble2);
		Alg scramble2 = new Alg("R' D' B L' D2 F' L2 D' B' L2 U' F' L' D F L2 D B' R' D2 F2");
		solver.setScramble(scramble2);
		solver.setSubOptimalCrossDegree(1);
		Profile.check();
		Alg result = solver.solve();
		Profile.check();
		System.out.println(PrettySolution.solutionInChunks(scramble2, result));
		System.out.println(result);

	}

	public Alg solve() {
		Cube cube = new Cube(this.scramble);
		ArrayList<Alg> crossSolutions = XCross.getCrossesNoX(cube, this.subOptimalCrossDegree);
		System.out.println();
		this.crossesSearched = 0;
		this.nodesSearched = 0;
		this.totalCrosses = crossSolutions.size();
		for (Alg cross : crossSolutions) {
			if (this.killMe) {
				return bestAlg;
			}
			Cube nextCube = new Cube(cube);
			nextCube.transform(cross);
			ArrayList<Alg> a = new ArrayList<Alg>();
			a.add(cross);
			this.solveASlot(nextCube, a);
			this.crossesSearched++;
		}
		this.finished = true;
		return bestAlg;
	}

	@Override
	public void run() {
		solve();
	}

	public void setAllowXCross(boolean b) {
		this.allowXCross = b;
	}
}

package com.ericvogler.CubePractice01;

public enum F2lPair {
	FR(Edge.FR, Corner.DRF), FL(Edge.FL, Corner.DFL), BR(Edge.BR, Corner.DBR), BL(
			Edge.BL, Corner.DLB);

	public Edge edge;
	public Corner corner;

	F2lPair(Edge edge, Corner corner) {
		this.edge = edge;
		this.corner = corner;
	}


	public static void main(String[] args) {
		for (F2lPair x: F2lPair.values())
			System.out.println(x);
	}
}

package com.ericvogler.CubePractice01;

public class EdgeLocation {
	public int edgePermutation;
	public Flip edgeOrientation;

	public EdgeLocation(int edgePermutation, Flip edgeOrientation) {
		this.edgePermutation = edgePermutation;
		this.edgeOrientation = edgeOrientation;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((edgeOrientation == null) ? 0 : edgeOrientation.hashCode());
		result = prime * result + edgePermutation;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EdgeLocation other = (EdgeLocation) obj;
		if (edgeOrientation != other.edgeOrientation)
			return false;
		if (edgePermutation != other.edgePermutation)
			return false;
		return true;
	}

}

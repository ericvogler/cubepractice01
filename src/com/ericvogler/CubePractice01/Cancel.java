package com.ericvogler.CubePractice01;

import java.util.ArrayList;
import java.util.Arrays;

public class Cancel {

	public static Alg cancelledAlg(Alg alg) {
		Alg lastStep = alg;
		Alg nextStep = oneStepCancelledAlg(alg);
		while (!lastStep.equals(nextStep)) {
			lastStep = nextStep;
			nextStep = oneStepCancelledAlg(nextStep);
		}
		return nextStep;
	}

	private static Alg oneStepCancelledAlg(Alg alg) {
		if (alg.getLength() == 0) {
			return alg;
		}
		Move[][] splitMoves = splitMovesByAxis(alg.getMoves());
		Move[][] reducedMoves = new Move[splitMoves.length][];
		for (int i = 0; i < reducedMoves.length; i++)
			reducedMoves[i] = cancelAxis(splitMoves[i]);
		Alg cancelled = new Alg(joinMoves(reducedMoves));
		return cancelled;
	}

	private static Move[] joinMoves(Move[][] reducedMoves) {
		int length = 0;
		for (Move[] b : reducedMoves)
			length += b.length;
		Move[] result = new Move[length];
		int resultPos = 0;
		for (Move[] b : reducedMoves) {
			for (int i = 0; i < b.length; i++)
				result[resultPos + i] = b[i];
			resultPos += b.length;
		}
		return result;
	}

	private static Move[] cancelAxis(Move[] splitMoves) {
		if (splitMoves.length == 1) {
			return splitMoves;
		}
		Move firstLayer = splitMoves[0].getLayer();
		int[] qTurns = { 0, 0 };
		for (Move m : splitMoves) {
			if (m.getLayer() == firstLayer)
				qTurns[0] += m.getClockwiseQTM();
			else
				qTurns[1] += m.getClockwiseQTM();
		}
		int resultLength = 0;
		for (int i = 0; i < 2; i++) {
			qTurns[i] = qTurns[i] % 4;
			if (qTurns[i] != 0)
				resultLength++;
		}
		Move[] result = new Move[resultLength];
		int resultPos = 0;
		for (int i = 0; i < 2; i++) {
			if (qTurns[i] != 0) {
				Move layer;
				if (i == 0)
					layer = firstLayer;
				else
					layer = firstLayer.getOppositeLayer();
				result[resultPos] = moveFromLayerAndTurns(layer, qTurns[i]);
				resultPos++;
			}
		}
		return result;
	}

	private static Move moveFromLayerAndTurns(Move layer, int clockwiseQTM) {

		switch (clockwiseQTM) {
		case 1:
			switch (layer) {
			case R:
				return Move.R;
			case L:
				return Move.L;
			case U:
				return Move.U;
			case D:
				return Move.D;
			case F:
				return Move.F;
			case B:
				return Move.B;
			}
		case 2:
			switch (layer) {
			case R:
				return Move.R2;
			case L:
				return Move.L2;
			case U:
				return Move.U2;
			case D:
				return Move.D2;
			case F:
				return Move.F2;
			case B:
				return Move.B2;
			}
		case 3:
			switch (layer) {
			case R:
				return Move.Rp;
			case L:
				return Move.Lp;
			case U:
				return Move.Up;
			case D:
				return Move.Dp;
			case F:
				return Move.Fp;
			case B:
				return Move.Bp;
			}
		}
		return null;
	}

	private static Move[][] splitMovesByAxis(Move[] moves) {
		ArrayList<Integer> groups = new ArrayList<Integer>();
		groups.add(0);
		Move currentGroup = moves[0].getAxisGroup();
		for (int i = 1; i < moves.length; i++) {
			Move g = moves[i].getAxisGroup();
			if (g != currentGroup || g == null) {
				groups.add(i);
				currentGroup = g;
			}
		}

		Move[][] result = new Move[groups.size()][];
		for (int i = 0; i < (result.length - 1); i++)
			result[i] = Arrays.copyOfRange(moves, groups.get(i),
					groups.get(i + 1));
		result[result.length - 1] = Arrays.copyOfRange(moves,
				groups.get(result.length - 1), moves.length);
		return result;
	}

	public static void main(String[] args) {
		Alg alg = new Alg("R U R' L R x x y2 y U R L'");
		Alg alg2 = cancelledAlg(alg);
		System.out.println(alg.getNotation());
		System.out.println(alg2.getNotation());
	}

}

package com.ericvogler.CubePractice01;

import java.util.HashSet;
import java.util.Random;

public class Mask {
	private Masked[] edgePermutation;
	private Masked[] cornerPermutation;
	private Masked[] edgeOrientation;
	private Masked[] cornerOrientation;
	private Masked centerOrientation;

	public Mask(Masked[] edgePermutation, Masked[] cornerPermutation,
			Masked[] edgeOrientation, Masked[] cornerOrientation,
			Masked centerOrientation) {
		this.edgePermutation = edgePermutation;
		this.cornerPermutation = cornerPermutation;
		this.edgeOrientation = edgeOrientation;
		this.cornerOrientation = cornerOrientation;
		this.centerOrientation = centerOrientation;
	}

	public Mask(Masked[] edges, Masked[] corners, Masked center) {
		this.edgePermutation = edges;
		this.edgeOrientation = edges;
		this.cornerPermutation = corners;
		this.cornerOrientation = corners;
		this.centerOrientation = center;
	}

	public Mask(Edge[] edges, Corner[] corners, Masked center) {
		Mask mask = getFullkMask();
		this.edgePermutation = mask.edgePermutation;
		this.edgeOrientation = mask.edgeOrientation;
		this.cornerPermutation = mask.cornerPermutation;
		this.cornerOrientation = mask.cornerOrientation;
		this.centerOrientation = center;

		for (Edge edge : edges) {
			this.edgeOrientation[edge.ordinal()] = Masked.UNMASKED;
			this.edgePermutation[edge.ordinal()] = Masked.UNMASKED;
		}
		for (Corner corner : corners) {
			this.cornerOrientation[corner.ordinal()] = Masked.UNMASKED;
			this.cornerPermutation[corner.ordinal()] = Masked.UNMASKED;
		}

	}

	public Mask(Cube cube, Edge[] edges, Corner[] corners, Masked center) {
		HashSet<Edge> edgeSet = new HashSet<Edge>();
		HashSet<Corner> cornerSet = new HashSet<Corner>();
		// ? Way to do this all at once?
		for (Edge edge : edges) {
			edgeSet.add(edge);
		}
		for (Corner corner : corners) {
			cornerSet.add(corner);
		}

		Edge[] ep = cube.getEdgePermutation();
		Corner[] cp = cube.getCornerPermutation();

		Masked[] edgeResult = new Masked[12];
		Masked[] cornerResult = new Masked[8];
		Masked centerResult;

		for (int i = 0; i < 12; i++) {
			edgeResult[i] = (edgeSet.contains(ep[i])) ? Masked.UNMASKED
					: Masked.MASKED;
		}
		for (int i = 0; i < 8; i++) {
			cornerResult[i] = (cornerSet.contains(cp[i])) ? Masked.UNMASKED
					: Masked.MASKED;
		}
		if (cube.getCenterOrientation() == -1) {
			centerResult = Masked.MASKED;
		} else {
			centerResult = center;
		}

		this.edgePermutation = edgeResult;
		this.edgeOrientation = edgeResult;
		this.cornerPermutation = cornerResult;
		this.cornerOrientation = cornerResult;
		this.centerOrientation = centerResult;

	}

	public static Mask getFullkMask() {
		return new Mask(new Masked[] { Masked.MASKED, Masked.MASKED,
				Masked.MASKED, Masked.MASKED, Masked.MASKED, Masked.MASKED,
				Masked.MASKED, Masked.MASKED, Masked.MASKED, Masked.MASKED,
				Masked.MASKED, Masked.MASKED, }, new Masked[] { Masked.MASKED,
				Masked.MASKED, Masked.MASKED, Masked.MASKED, Masked.MASKED,
				Masked.MASKED, Masked.MASKED, Masked.MASKED, }, Masked.MASKED);
	}

	public Masked[] getEdgePermutation() {
		return edgePermutation;
	}

	public Masked[] getCornerPermutation() {
		return cornerPermutation;
	}

	public Masked[] getEdgeOrientation() {
		return edgeOrientation;
	}

	public Masked[] getCornerOrientation() {
		return cornerOrientation;
	}

	public Masked getCenterOrientation() {
		return centerOrientation;
	}

	public static Mask getRandomMask() {
		Random random = new Random();
		Masked[] edges = new Masked[12];
		Masked[] corners = new Masked[8];
		for (int i = 0; i < 12; i++)
			if (random.nextBoolean()) {
				edges[i] = Masked.UNMASKED;
			} else {
				edges[i] = Masked.MASKED;
			}
		for (int i = 0; i < 8; i++)
			if (random.nextBoolean()) {
				corners[i] = Masked.UNMASKED;
			} else {
				corners[i] = Masked.MASKED;
			}
		edges[0] = Masked.MASKED;
		Mask mask = new Mask(edges, corners, Masked.UNMASKED);
		return mask;
	}
}

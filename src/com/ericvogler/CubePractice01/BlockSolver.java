package com.ericvogler.CubePractice01;

import java.util.ArrayList;

public class BlockSolver {

	private Cube startCube;
	private Cube desiredCube;

	public BlockSolver(MaskedCube startCube, MaskedCube desiredCube) {
		this.startCube = startCube;
		this.desiredCube = desiredCube;
	}

	public BlockSolver(Cube cube, Piece[] pieces) {
		Edge[] edges = Edge.edges(pieces);
		Corner[] corners = Corner.corners(pieces);
		Mask startMask = new Mask(cube, edges, corners, Masked.UNMASKED);
		Mask endMask = new Mask(edges, corners, Masked.UNMASKED);
		this.startCube = new MaskedCube(cube, startMask);
		this.desiredCube = new MaskedCube(endMask);
	}

	public ArrayList<Alg> getSolutions() {
		if (startCube.equals(desiredCube)) {
			ArrayList<Alg> blankList = new ArrayList<Alg>();
			blankList.add(new Alg(""));
			return blankList;
		}
		int moveCount = 1;
		do {
			ArrayList<Alg> x = getSolutions(moveCount);
			if (x.size() > 0) {
				System.out.print(".");
				return x;
			}
			moveCount++;
		} while (true);
	}

	public ArrayList<Alg> getSolutions(int moveCount) {
		boolean savedCacheSetting = Transformations.isCacheResults();
		Transformations.setCacheResults(false);
		System.out.print(moveCount);
		ArrayList<Alg> result = new ArrayList<Alg>();

		MovesIterator mi;
		if (moveCount == 1) {
			mi = new MovesIterator(1);
			do {

				Alg a = new Alg(mi.getNext());
				MaskedCube cube = new MaskedCube(startCube);
				cube.transform(a);
				if (cube.equals(desiredCube)) {
					result.add(a);
				}
			} while (!mi.isFinished());
			Transformations.setCacheResults(savedCacheSetting);
			return result;
		} else {
			int tableDepth = (moveCount + 1) / 2;
			int searchDepth = moveCount - tableDepth;
			mi = new MovesIterator(searchDepth);

			LookupTable lt = new LookupTable(desiredCube, tableDepth);
			do {
				Alg a = new Alg(mi.getNext());
				MaskedCube cube = new MaskedCube(startCube);
				cube.transform(a);
				ArrayList<Alg> nextResult = lt.getAlgs(cube);
				if (nextResult != null)
					for (Alg i : nextResult) {
						result.add(new Alg(new Alg[] { a, i }));
					}

			} while (!mi.isFinished());
			Transformations.setCacheResults(savedCacheSetting);
			return result;
		}
	}

	public static Piece[][] incrementalPieces(Piece[][] pieceSteps) {
		// XXX: I assume this is really lame and would be much cleaner with
		// ArrayLists.
		// TODO: Try refactoring a bunch of array stuffs to ArrayLists.
		Piece[][] result = new Piece[pieceSteps.length][];
		Piece[] accumulator = {};
		Piece[] nextAccumulator;
		int pos = 0;
		for (Piece[] pieceStep : pieceSteps) {
			nextAccumulator = new Piece[accumulator.length + pieceStep.length];
			for (int i = 0; i < accumulator.length; i++) {
				nextAccumulator[i] = accumulator[i];
			}
			for (int i = 0; i < pieceStep.length; i++) {
				nextAccumulator[i + accumulator.length] = pieceStep[i];
			}
			result[pos++] = nextAccumulator;
			accumulator = nextAccumulator;
		}
		return result;
	}

	public static void main(String[] args) {

		Piece[][] pieceSteps = incrementalPieces(new Piece[][] {
				{ Edge.DL, Edge.DB }, { Edge.DF, Edge.DR },
				{ Corner.DBR, Edge.BR }, { Corner.DLB, Edge.BL },
				{ Corner.DFL, Edge.FL }, { Corner.DRF, Edge.FR },
				{ Edge.UB, Edge.UF, Edge.UL, Edge.UR },
		// { Corner.UBL},
		// {Corner.ULF, Corner.URB, Corner.UFR }
		});
		// Alg scramble = new Scrambler().nextScramble();
		Alg scramble = new Alg(
				"F R U' L' F2 D2 R' F2 D2 F D' B2 U' L2 F2 L2 U' L2 F2");

		Profile.check();
		ArrayList<Alg> solutions = solvePiecesInSteps(new Cube(scramble),
				pieceSteps);
		Profile.check();
		System.out.println();
		System.out.println(scramble);
		for (Alg step : solutions) {
			System.out.println(step);
		}
		System.out.println("Solved!");
	}

	public static ArrayList<Alg> solvePiecesInSteps(Cube cube,
			Piece[][] pieceSteps) {
		Cube startCube = new Cube(cube);
		ArrayList<Alg> solutions = new ArrayList<Alg>();

		for (Piece[] pieceStep : pieceSteps) {
			BlockSolver bs = new BlockSolver(startCube, pieceStep);
			Alg solution = bs.getSolutions().get(0);
			startCube.transform(solution);
			solutions.add(solution);
		}
		return solutions;
	}

	public static String oneQuestion() {
		Scrambler scrambler = new Scrambler();
		Alg scramble = scrambler.getScramble();

		Mask mask = new Mask(new Edge[] { Edge.DF, Edge.DL }, new Corner[] {},
				Masked.MASKED);
		MaskedCube secondCube = new MaskedCube(mask);

		Mask mask2 = new Mask(
				new Edge[] { Edge.DF, Edge.DL, Edge.DR, Edge.DF },
				new Corner[] {}, Masked.MASKED);
		MaskedCube thirdCube = new MaskedCube(mask2);

		MaskedCube startCube = new MaskedCube(secondCube);

		startCube.transform(scramble);
		BlockSolver blockSolver = new BlockSolver(startCube, secondCube);
		ArrayList<Alg> solutions = blockSolver.getSolutions();
		Alg solution = solutions.get(0);

		startCube = new MaskedCube(thirdCube);
		startCube.transform(scramble);
		startCube.transform(solution);
		BlockSolver bs2 = new BlockSolver(startCube, thirdCube);
		solutions = bs2.getSolutions();
		Alg solution2 = solutions.get(0);

		System.out.println(scramble.getNotation());
		System.out.print("RESULTS: ");
		System.out.println(solutions.size());
		System.out.print("HTM: ");
		System.out.println(solution.HTM());
		System.out.println(solution.getNotation());
		System.out.println(solution2.getNotation());

		String result = scramble.getNotation() + "\n" + solution.getNotation()
				+ "\n" + solution2.getNotation();
		return result;
	}
}

package com.ericvogler.CubePractice01;

import java.util.ArrayList;

public enum Edge implements Piece {
	UF, UR, UB, UL, DF, DR, DB, DL, FR, FL, BR, BL;

	public static Edge[] edges(Piece[] pieces) {
		ArrayList<Edge> result = new ArrayList<Edge>();
		for (Piece piece : pieces) {
			if (piece instanceof Edge) {
				result.add((Edge) piece);
			}
		}
		Edge[] resultArray = new Edge[result.size()];
		for (int i = 0; i < resultArray.length; i++) {
			resultArray[i] = result.get(i);
		}
		return resultArray;
	}
}

package com.ericvogler.CubePractice01;

public enum Move {
	R, R2, Rp, L, L2, Lp, U, U2, Up, D, D2, Dp, F, F2, Fp, B, B2, Bp, Rw, Rw2, Rwp, Lw, Lw2, Lwp, Uw, Uw2, Uwp, Dw, Dw2, Dwp, Fw, Fw2, Fwp, Bw, Bw2, Bwp, m, m2, mp, e, e2, ep, s, s2, sp, x, x2, xp, y, y2, yp, z, z2, zp;

	private Move reverseMove = null;
	private Move axisGroup = null;
	private Move layer = null;
	private Move oppositeLayer = null;
	private int clockwiseQTM = 0;

	static {
		R.reverseMove = Rp;
		Rp.reverseMove = R;
		L.reverseMove = Lp;
		Lp.reverseMove = L;
		U.reverseMove = Up;
		Up.reverseMove = U;
		D.reverseMove = Dp;
		Dp.reverseMove = D;
		F.reverseMove = Fp;
		Fp.reverseMove = F;
		B.reverseMove = Bp;
		Bp.reverseMove = B;
	}

	static {
		R.axisGroup = R;
		R.axisGroup = R;
		Rp.axisGroup = R;
		R2.axisGroup = R;
		L.axisGroup = R;
		Lp.axisGroup = R;
		L2.axisGroup = R;

		U.axisGroup = U;
		Up.axisGroup = U;
		U2.axisGroup = U;
		D.axisGroup = U;
		D2.axisGroup = U;
		Dp.axisGroup = U;

		F.axisGroup = F;
		F2.axisGroup = F;
		Fp.axisGroup = F;
		B.axisGroup = F;
		B2.axisGroup = F;
		Bp.axisGroup = F;
	}

	static {
		for (Move move : new Move[] { R, R2, Rp })
			move.layer = R;
		for (Move move : new Move[] { L, L2, Lp })
			move.layer = L;
		for (Move move : new Move[] { U, U2, Up })
			move.layer = U;
		for (Move move : new Move[] { D, D2, Dp })
			move.layer = D;
		for (Move move : new Move[] { F, F2, Fp })
			move.layer = F;
		for (Move move : new Move[] { B, B2, Bp })
			move.layer = B;
	}

	static {
		for (Move move : Move.values()) {
			if (move.layer != null) {
				switch (move.layer) {
				case R:
					move.oppositeLayer = L;
					break;
				case L:
					move.oppositeLayer = R;
					break;
				case U:
					move.oppositeLayer = D;
					break;
				case D:
					move.oppositeLayer = U;
					break;
				case F:
					move.oppositeLayer = B;
					break;
				case B:
					move.oppositeLayer = F;
					break;
				}
			}
		}
	}

	static {
		for (Move move : new Move[] { R, L, U, D, F, B })
			move.clockwiseQTM = 1;
		for (Move move : new Move[] { R2, L2, U2, D2, F2, B2 })
			move.clockwiseQTM = 2;
		for (Move move : new Move[] { Rp, Lp, Up, Dp, Fp, Bp })
			move.clockwiseQTM = 3;
	}

	public Move getReverse() {
		return (reverseMove == null) ? this : reverseMove;
	}

	public Move getAxisGroup() {
		return axisGroup;
	}

	public Move getLayer() {
		return layer;
	}

	public Move getOppositeLayer() {
		return oppositeLayer;
	}

	public int getClockwiseQTM() {
		return clockwiseQTM;
	}

	public static void main(String[] args) {
		Move[] moves = new Move[] { R, Lp, D2 };
		for (Move move : moves) {
			System.out.println(move.getReverse());
		}
		System.out.println(Move.x.getAxisGroup());
	}
}

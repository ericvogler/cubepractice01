package com.ericvogler.CubePractice01;

public class F2lPairWithLocation {
	//It seems weird to have a new class with a new file every time I want to pair two things together.
	
	public final F2lPair f2lPair;
	public final PairLocation pairLocation;
	
	public F2lPairWithLocation(F2lPair f2lPair, PairLocation pairLocation) {
		this.f2lPair = f2lPair;
		this.pairLocation = pairLocation;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((f2lPair == null) ? 0 : f2lPair.hashCode());
		result = prime * result
				+ ((pairLocation == null) ? 0 : pairLocation.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		F2lPairWithLocation other = (F2lPairWithLocation) obj;
		if (f2lPair != other.f2lPair)
			return false;
		if (pairLocation == null) {
			if (other.pairLocation != null)
				return false;
		} else if (!pairLocation.equals(other.pairLocation))
			return false;
		return true;
	}
	
	
}

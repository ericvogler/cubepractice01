package com.ericvogler.CubePractice01;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class LookupTable {
	/*
	 * initialize it with desired state and (set of algs|move count) LookupTable
	 * iterates through all algs, storing result: (reversed) alg
	 * 
	 * Then you can give it a cube and it tells you if that cube is in its
	 * table, and if so returns an array of algs that will get you to the
	 * original state;
	 */
	// Note: probably should have a HashMap<Cube, ArrayList<Alg>> class for
	// basic behavior;

	private Map<Cube, ArrayList<Alg>> theTable; // Tongue in cheek
												// name. Not sure
												// what would be a good
												// one.

	public LookupTable(Cube desiredCube, int moveCount) {
		// Map<Byte, String> tempMap = new HashMap<Byte, String>();
		// tempMap.put(Moves.R, "R");
		theTable = new HashMap<Cube, ArrayList<Alg>>();
		MovesIterator mi = new MovesIterator(moveCount);
		Alg nextAlg;
		Cube cube;
		do {
			nextAlg = new Alg(mi.getNext());
			// XXX: The whole Cube vs. MaskedCube thing seems lame, if I have to
			// make Cubes MaskedCubes just to get them to work in the HashMap. 
			cube = new MaskedCube(desiredCube);
			cube.transform(nextAlg);
			nextAlg.reverse();
			// next bit should be method
			if (theTable.containsKey(cube)) {
				ArrayList<Alg> a = theTable.get(cube);
				a.add(nextAlg);
				theTable.put(cube, a);
			} else {
				ArrayList<Alg> a = new ArrayList<Alg>();
				a.add(nextAlg);
				theTable.put(cube, a);
			}

		} while (!mi.isFinished());
	}

	public ArrayList<Alg> getAlgs(Cube cube) {
		if (this.theTable.containsKey(cube))
			return theTable.get(cube);
		else
			return null;
	}

	public static void main(String[] args) {
		Masked[] edges = { Masked.MASKED, Masked.MASKED, Masked.MASKED,
				Masked.MASKED, Masked.MASKED, Masked.MASKED, Masked.MASKED,
				Masked.MASKED, Masked.UNMASKED, Masked.UNMASKED,
				Masked.UNMASKED, Masked.UNMASKED, };
		Masked[] corners = { Masked.MASKED, Masked.MASKED, Masked.MASKED,
				Masked.MASKED, Masked.MASKED, Masked.MASKED, Masked.MASKED,
				Masked.MASKED, };
		Mask mask = new Mask(edges, corners, Masked.MASKED);
		MaskedCube cube = new MaskedCube(mask);
		cube.printState();

		LookupTable lt = new LookupTable(cube, 4);
		// cube.transform(new Alg("R2 D2"));
		ArrayList<Alg> answer = lt.getAlgs(cube);
		if (answer == null)
			System.out.println("null");
		else
			System.out.println(answer.size());

		Alg x = answer.get(0);

		System.out.println(x.getNotation());

	}
}

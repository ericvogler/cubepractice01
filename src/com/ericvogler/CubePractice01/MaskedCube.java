package com.ericvogler.CubePractice01;

public class MaskedCube extends Cube {

	public MaskedCube(Cube cube, Mask mask) {
		super(cube);
		applyMask(mask);
	}

	public MaskedCube(Cube cube) {
		super(cube);
	}
	
	public MaskedCube(Mask mask) {
		super();
		applyMask(mask);
	}
	
	public MaskedCube(MaskedCube maskedCube) {
		//XXX: This might be wrong;
		super(maskedCube);
	}
	
	private void applyMask(Mask mask) {
		for (int i = 0; i < 12; i++) {
			if (mask.getEdgeOrientation()[i] == Masked.MASKED) {
				this.edgeOrientation[i] = null;
			}
			if (mask.getEdgePermutation()[i] == Masked.MASKED) {
				this.edgePermutation[i] = null;
			}
		}
		for (int i = 0; i < 8; i++) {
			if (mask.getCornerOrientation()[i] == Masked.MASKED) {
				this.cornerOrientation[i] = null;
			}
			if (mask.getCornerPermutation()[i] == Masked.MASKED) {
				this.cornerPermutation[i] = null;
			}
		}
	}

	public static void main(String[] args) {
		Cube cube = new Cube();
		cube.printState();

		Masked[] edges = new Masked[12];
		Masked[] corners = new Masked[8];
		for (int i=0;i<12;i++)
			edges[i] = Masked.UNMASKED;
		for (int i=0;i<8;i++)
			corners[i] = Masked.UNMASKED;
		edges[0] = Masked.MASKED;
		Mask mask = new Mask(edges, corners, Masked.UNMASKED);
		MaskedCube mCube = new MaskedCube(cube, mask);
		mCube.printState();
	}
}

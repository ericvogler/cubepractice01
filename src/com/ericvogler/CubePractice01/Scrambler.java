package com.ericvogler.CubePractice01;
import java.util.Random;

public class Scrambler {
	//TODO: Smarter scramble!
	public Scrambler() { }

	private Move randomFromGroup(int group){
		Move[][] options =  {{Move.R, Move.R2, Move.Rp, Move.L, Move.L2, Move.Lp}, 
				{Move.U, Move.U2, Move.Up, Move.D, Move.D2, Move.Dp},		
				{Move.F, Move.F2, Move.Fp, Move.B, Move.B2, Move.Bp}};
		Random random = new Random();
		return options[group][random.nextInt(6)];
	}

	public Alg getScramble(int length) {
		Move[] scramble = new Move[length];
		for (int i=0; i<length; i++)
			scramble[i] = randomFromGroup(i%3);
		return new Alg(scramble);
	}
	
	public Alg getScramble() {
		return getScramble(21);
	}
	
	public Alg getCrossScramble() {
		Alg regularScramble = getScramble(15);
		Alg solution = XCross.getCrosses(new Cube(regularScramble), 0).get(0);
		return Cancel.cancelledAlg(new Alg(new Alg[] {regularScramble, solution}));
		
	}
	
	public static void main(String[] args) {
		Scrambler scrambler = new Scrambler();
		System.out.println(scrambler.getCrossScramble());
	}

	
}

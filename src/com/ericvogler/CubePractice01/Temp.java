package com.ericvogler.CubePractice01;

import java.util.HashSet;

public class Temp {

	private static boolean startsOrEndsWithU(Alg alg) {
		Move[] moves = alg.getMoves();
		if ((moves[0].getLayer() == Move.U) ||
				(moves[0].getLayer() == Move.D)
				|| (moves[moves.length - 1].getLayer() == Move.U)
				|| (moves[moves.length - 1].getLayer() == Move.D)) {
			return true;
		}
		return false;
	}

	public static void main(String[] args) {

		HashSet<Alg> normalHash = new HashSet<Alg>();
		for (Alg alg : F2lMoves.allDoubleAlgs()) {
			normalHash.add(alg);
		}

		Alg[] x = F2lMoves.nonStandardF2Ls(5);

		HashSet<Alg> cardinalSet = new HashSet<Alg>();
		for (Alg alg : x) {
			if (!normalHash.contains(alg)) {
				if (!startsOrEndsWithU(alg)) {
					boolean insert = true;
					for (Alg variation : F2lMoves
							.allRotationsMirrorsAndInversions(alg)) {
						if (cardinalSet.contains(variation)) {
							insert = false;
						}
					}
					if (insert) {
						cardinalSet.add(alg);
						System.out.print("\"");
						System.out.print(alg);
						System.out.println("\",");						
					}
				}
			}
		}

	}
}